package felipe.wizardchess.ui;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Toast;

import com.qualcomm.vuforia.CameraDevice;
import com.qualcomm.vuforia.Vuforia;

import java.util.Vector;

import felipe.wizardchess.R;
import felipe.wizardchess.application.chess.ChessSet;
import felipe.wizardchess.application.controllers.AppException;
import felipe.wizardchess.application.controllers.AppSession;
import felipe.wizardchess.application.controllers.IGameController;
import felipe.wizardchess.application.speechrecognition.SpeechRecognizer;
import felipe.wizardchess.ui.utils.AppGLView;
import felipe.wizardchess.ui.utils.LoadingDialogHandler;
import felipe.wizardchess.ui.utils.Texture;


public class GameActivity extends ActionBarActivity {
    private static final String LOGTAG = "WizardChess Activity";

    // Our OpenGL view:
    private AppGLView appGlView;

    // Our renderer:
    private GameActivityRender renderer;

    private GestureDetector gestureDetector;

    // The textures we will use for rendering:
    private Vector<Texture> textures;

    private FrameLayout UILayout;

    LoadingDialogHandler loadingDialogHandler = new LoadingDialogHandler(this);

    boolean isDroidDevice = false;

    SpeechRecognizer speechRecognizer;

    IGameController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(controller == null) {
            controller = new IGameController(this);
            controller.doInit();
        }

        gestureDetector = new GestureDetector(this, new GestureListener());

        // Load any sample specific textures:
        textures = new Vector<Texture>();
        loadTextures();

        isDroidDevice = android.os.Build.MODEL.toLowerCase().startsWith("droid");

        startLoadingAnimation();
    }

    // Process Single Tap event to trigger autofocus
    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        // Used to set autofocus one second after a manual focus is triggered
        private final Handler autofocusHandler = new Handler();

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            // Generates a Handler to trigger autofocus
            // after 1 second
            autofocusHandler.postDelayed(new Runnable() {
                public void run() {
                    boolean result = CameraDevice.getInstance().setFocusMode(
                            CameraDevice.FOCUS_MODE.FOCUS_MODE_TRIGGERAUTO);

                    if (!result)
                        Log.e("SingleTapUp", "Unable to trigger focus");
                }
            }, 1000L);

            return true;
        }
    }

    public void loadUiLayout() {
        renderer.mIsActive = true;
        UILayout.addView(appGlView, 0, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        UILayout.setBackgroundColor(Color.TRANSPARENT);

        View view = UILayout.findViewById(R.id.lay_controls);
        view.setVisibility(View.VISIBLE);
    }

    public void initGL(AppSession appSession, ChessSet chessSet) {
        // Create OpenGL ES view:
        int depthSize = 16;
        int stencilSize = 0;
        boolean translucent = Vuforia.requiresAlpha();

        appGlView = new AppGLView(this);
        appGlView.init(translucent, depthSize, stencilSize);

        renderer = new GameActivityRender(this, appSession, chessSet);
        //renderer.setTextures(R.drawable.textb, R.drawable.textw);
        appGlView.setRenderer(renderer);
    }


    private void loadTextures() {
        textures.add(Texture.loadTextureFromApk("ChessPieces/textB.png", getAssets()));
        textures.add(Texture.loadTextureFromApk("ChessPieces/textW.png", getAssets()));
    }

    // Called when the activity will start interacting with the user.
    @Override
    protected void onResume() {
        Log.d(LOGTAG, "onResume");
        super.onResume();

        // This is needed for some Droid devices to force portrait
        if (isDroidDevice) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        try {
            controller.resumeAR();
        } catch (AppException e) {
            Log.e(LOGTAG, e.getString());
        }

        // Resume the GL view:
        if (appGlView != null) {
            appGlView.setVisibility(View.VISIBLE);
            appGlView.onResume();
        }

    }


    // Callback for configuration changes the activity handles itself
    @Override
    public void onConfigurationChanged(Configuration config) {
        Log.d(LOGTAG, "onConfigurationChanged");
        super.onConfigurationChanged(config);

        controller.onConfigurationChanged();
    }


    // Called when the system is about to start resuming a previous activity.
    @Override
    protected void onPause() {
        Log.d(LOGTAG, "onPause");
        super.onPause();

        if (appGlView != null) {
            appGlView.setVisibility(View.INVISIBLE);
            appGlView.onPause();
        }

        try {
            controller.pauseAR();
        } catch (AppException e) {
            Log.e(LOGTAG, e.getString());
        }
    }


    // The final call you receive before your activity is destroyed.
    @Override
    protected void onDestroy() {
        Log.d(LOGTAG, "onDestroy");
        super.onDestroy();

        try {
            controller.stopAR();
        } catch (AppException e) {
            Log.e(LOGTAG, e.getString());
        }

        // Unload texture:
        textures.clear();
        textures = null;

        System.gc();
    }

    private void startLoadingAnimation() {
        setContentView(R.layout.activity_game);
        UILayout = (FrameLayout) findViewById(R.id.game_layout);

        UILayout.setVisibility(View.VISIBLE);
        UILayout.setBackgroundColor(Color.BLACK);

        // Initialize components actions
        SeekBar sbScale = (SeekBar) findViewById(R.id.sb_scale);
        sbScale.setProgress((int) GameActivityRender.getGlobalScaleFloat());
        sbScale.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        sbScale.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                GameActivityRender.setGlobalScaleFloat(progress);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        // Gets a reference to the loading dialog
        loadingDialogHandler.mLoadingDialogContainer = UILayout.findViewById(R.id.loading_indicator);

        // Shows the loading indicator at start
        loadingDialogHandler.sendEmptyMessage(LoadingDialogHandler.SHOW_LOADING_DIALOG);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Process the Gestures
        gestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    final public static int CMD_BACK = -1;
    final public static int CMD_EXTENDED_TRACKING = 1;
    final public static int CMD_AUTOFOCUS = 2;
    final public static int CMD_FLASH = 3;
    final public static int CMD_CAMERA_FRONT = 4;
    final public static int CMD_CAMERA_REAR = 5;
    final public static int CMD_DATASET_START_INDEX = 6;

    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    public void onListen(View view) {
        controller.getMoveFromSpeech();
    }

    public void blockControlls() {
        View btn = findViewById(R.id.hearing_button);
        btn.setActivated(false);
    }

    public void freeControlls() {
        View btn = findViewById(R.id.hearing_button);
        btn.setActivated(true);
    }

    public SpeechRecognizer getSpeech() {
        SpeechRecognizer speechRecognizer = new SpeechRecognizer(this);
        speechRecognizer.setProgressBar((ProgressBar)findViewById(R.id.progress_sound));
        return speechRecognizer;
    }

}


/*
private static final String LOGTAG = "WizardChess Activity";
    private static final int ADVANCE_BY_SEC = 100;
    private int frameCount = 0;
    Runnable action;

    AppSession appSession;

    private DataSet currentDataset;
    private int currentDatasetSelectionIndex = 0;
    private int startDatasetsIndex = 0;
    private int datasetsNumber = 0;
    private ArrayList<String> datasetStrings = new ArrayList<String>();

    // Our OpenGL view:
    private AppGLView appGlView;

    // Our renderer:
    private GameActivityRender renderer;

    private GestureDetector gestureDetector;

    // The textures we will use for rendering:
    private Vector<Texture> textures;

    private boolean switchDatasetAsap = false;
    private boolean mContAutofocus = false;
    private boolean extendedTracking = true;

    private FrameLayout UILayout;

    LoadingDialogHandler loadingDialogHandler = new LoadingDialogHandler(this);

    // Alert Dialog used to display SDK errors
    private AlertDialog errorDialog;

    boolean isDroidDevice = false;

    // ChessPieces
    ChessSet chessSet;

    SpeechRecognizer speechRecognizer;

    enum GameMode {MULTPLAYER, ONEPLAYER};
    GameMode gameMode = GameMode.ONEPLAYER;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appSession = new AppSession(this);

        startLoadingAnimation();
        //datasetStrings.add("StonesAndChips.xml");
        //datasetStrings.add("Tarmac.xml");
        datasetStrings.add("WizardChessDB/WizardChessDB.xml");

        appSession.initAR(this, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        gestureDetector = new GestureDetector(this, new GestureListener());

        // Load any sample specific textures:
        textures = new Vector<Texture>();
        loadTextures();

        isDroidDevice = android.os.Build.MODEL.toLowerCase().startsWith("droid");

        chessSet = new ChessSet(this);
        chessSet.setSwitchListener(this);

        action = new Runnable() {
            @Override
            public void run() {
                advance();
            }
        };

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(action);
            }
        }, 1000, 1000/ADVANCE_BY_SEC);
    }

    // Process Single Tap event to trigger autofocus
    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        // Used to set autofocus one second after a manual focus is triggered
        private final Handler autofocusHandler = new Handler();

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            // Generates a Handler to trigger autofocus
            // after 1 second
            autofocusHandler.postDelayed(new Runnable() {
                public void run() {
                    boolean result = CameraDevice.getInstance().setFocusMode(
                            CameraDevice.FOCUS_MODE.FOCUS_MODE_TRIGGERAUTO);

                    if (!result)
                        Log.e("SingleTapUp", "Unable to trigger focus");
                }
            }, 1000L);

            return true;
        }
    }


    // We want to load specific textures from the APK, which we will later use
    // for rendering.

    private void loadTextures() {
        textures.add(Texture.loadTextureFromApk("ChessPieces/textB.png", getAssets()));
        textures.add(Texture.loadTextureFromApk("ChessPieces/textW.png", getAssets()));
    }


    // Called when the activity will start interacting with the user.
    @Override
    protected void onResume() {
        Log.d(LOGTAG, "onResume");
        super.onResume();

        // This is needed for some Droid devices to force portrait
        if (isDroidDevice) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        try {
            appSession.resumeAR();
        } catch (AppException e) {
            Log.e(LOGTAG, e.getString());
        }

        // Resume the GL view:
        if (appGlView != null) {
            appGlView.setVisibility(View.VISIBLE);
            appGlView.onResume();
        }

    }


    // Callback for configuration changes the activity handles itself
    @Override
    public void onConfigurationChanged(Configuration config) {
        Log.d(LOGTAG, "onConfigurationChanged");
        super.onConfigurationChanged(config);

        appSession.onConfigurationChanged();
    }


    // Called when the system is about to start resuming a previous activity.
    @Override
    protected void onPause() {
        Log.d(LOGTAG, "onPause");
        super.onPause();

        if (appGlView != null) {
            appGlView.setVisibility(View.INVISIBLE);
            appGlView.onPause();
        }

        try {
            appSession.pauseAR();
        } catch (AppException e) {
            Log.e(LOGTAG, e.getString());
        }
    }


    // The final call you receive before your activity is destroyed.
    @Override
    protected void onDestroy() {
        Log.d(LOGTAG, "onDestroy");
        super.onDestroy();

        try {
            appSession.stopAR();
        } catch (AppException e) {
            Log.e(LOGTAG, e.getString());
        }

        // Unload texture:
        textures.clear();
        textures = null;

        System.gc();
    }


    // Initializes AR application components.
    private void initApplicationAR() {
        // Create OpenGL ES view:
        int depthSize = 16;
        int stencilSize = 0;
        boolean translucent = Vuforia.requiresAlpha();

        appGlView = new AppGLView(this);
        appGlView.init(translucent, depthSize, stencilSize);

        renderer = new GameActivityRender(this, appSession, chessSet);
        //renderer.setTextures(R.drawable.textb, R.drawable.textw);
        appGlView.setRenderer(renderer);

    }


    private void startLoadingAnimation() {
        setContentView(R.layout.activity_game);
        UILayout = (FrameLayout) findViewById(R.id.game_layout);

        UILayout.setVisibility(View.VISIBLE);
        UILayout.setBackgroundColor(Color.BLACK);

        // Initialize components actions
        SeekBar sbScale = (SeekBar) findViewById(R.id.sb_scale);
        sbScale.setProgress((int) GameActivityRender.getGlobalScaleFloat());
        sbScale.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        sbScale.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                GameActivityRender.setGlobalScaleFloat(progress);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        // Gets a reference to the loading dialog
        loadingDialogHandler.mLoadingDialogContainer = UILayout.findViewById(R.id.loading_indicator);

        // Shows the loading indicator at start
        loadingDialogHandler.sendEmptyMessage(LoadingDialogHandler.SHOW_LOADING_DIALOG);

    }


    // Methods to load and destroy tracking data.
    @Override
    public boolean doLoadTrackersData() {
        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager
                .getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        if (currentDataset == null)
            currentDataset = objectTracker.createDataSet();

        if (currentDataset == null)
            return false;

        if (!currentDataset.load(
                datasetStrings.get(currentDatasetSelectionIndex),
                STORAGE_TYPE.STORAGE_APPRESOURCE))
            return false;

        if (!objectTracker.activateDataSet(currentDataset))
            return false;

        int numTrackables = currentDataset.getNumTrackables();
        for (int count = 0; count < numTrackables; count++) {
            Trackable trackable = currentDataset.getTrackable(count);
            if(isExtendedTrackingActive()) {
                trackable.startExtendedTracking();
            }

            String name = "Current Dataset : " + trackable.getName();
            trackable.setUserData(name);
            Log.d(LOGTAG, "UserData:Set the following user data "
                    + (String) trackable.getUserData());
        }

        return true;
    }


    @Override
    public boolean doUnloadTrackersData() {
        // Indicate if the trackers were unloaded correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager.getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        if (currentDataset != null && currentDataset.isActive()) {
            if (objectTracker.getActiveDataSet().equals(currentDataset)
                    && !objectTracker.deactivateDataSet(currentDataset)) {
                result = false;
            } else if (!objectTracker.destroyDataSet(currentDataset)) {
                result = false;
            }

            currentDataset = null;
        }

        return result;
    }


    @Override
    public void onInitARDone(AppException exception) {

        if (exception == null) {
            initApplicationAR();

            renderer.mIsActive = true;

            // Now add the GL surface view. It is important
            // that the OpenGL ES surface view gets added
            // BEFORE the camera is started and video
            // background is configured.
            //addContentView(appGlView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            UILayout.addView(appGlView, 0, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

            // Sets the UILayout to be drawn in front of the camera
            //UILayout.bringToFront();

            // Sets the layout background to transparent
            UILayout.setBackgroundColor(Color.TRANSPARENT);

            try {
                appSession.startAR(CameraDevice.CAMERA.CAMERA_DEFAULT);
            } catch (AppException e) {
                Log.e(LOGTAG, e.getString());
            }

            boolean result = CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO);

            if (result)
                mContAutofocus = true;
            else
                Log.e(LOGTAG, "Unable to enable continuous autofocus");

            View view = UILayout.findViewById(R.id.lay_controls);
            view.setVisibility(View.VISIBLE);

        } else {
            Log.e(LOGTAG, exception.getString());
            showInitializationErrorMessage(exception.getString());
        }
    }


    // Shows initialization error messages as System dialogs
    public void showInitializationErrorMessage(String message) {
        final String errorMessage = message;
        runOnUiThread(new Runnable() {
            public void run() {
                if (errorDialog != null) {
                    errorDialog.dismiss();
                }

                // Generates an Alert Dialog to show the error message
                AlertDialog.Builder builder = new AlertDialog.Builder( GameActivity.this);
                builder
                        .setMessage(errorMessage)
                        .setTitle(getString(R.string.INIT_ERROR))
                        .setCancelable(false)
                        .setIcon(0)
                        .setPositiveButton(getString(R.string.button_OK),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        finish();
                                    }
                                });

                errorDialog = builder.create();
                errorDialog.show();
            }
        });
    }


    @Override
    public void onQCARUpdate(State state) {
        if (switchDatasetAsap) {
            switchDatasetAsap = false;
            TrackerManager tm = TrackerManager.getInstance();
            ObjectTracker ot = (ObjectTracker) tm.getTracker(ObjectTracker .getClassType());
            if (ot == null || currentDataset == null || ot.getActiveDataSet() == null) {
                Log.d(LOGTAG, "Failed to swap datasets");
                return;
            }

            doUnloadTrackersData();
            doLoadTrackersData();
        }
    }


    @Override
    public boolean doInitTrackers() {
        // Indicate if the trackers were initialized correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        Tracker tracker;

        // Trying to initialize the image tracker
        tracker = tManager.initTracker(ObjectTracker.getClassType());
        if (tracker == null) {
            Log.e(
                    LOGTAG,
                    "Tracker not initialized. Tracker already initialized or the camera is already started");
            result = false;
        } else {
            Log.i(LOGTAG, "Tracker successfully initialized");
        }
        return result;
    }

    @Override
    public boolean doStartTrackers() {
        // Indicate if the trackers were started correctly
        boolean result = true;

        Tracker objectTracker = TrackerManager.getInstance().getTracker(ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.start();

        return result;
    }


    @Override
    public boolean doStopTrackers() {
        // Indicate if the trackers were stopped correctly
        boolean result = true;

        Tracker objectTracker = TrackerManager.getInstance().getTracker(
                ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.stop();

        return result;
    }


    @Override
    public boolean doDeinitTrackers() {
        // Indicate if the trackers were deinitialized correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        tManager.deinitTracker(ObjectTracker.getClassType());

        return result;
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Process the Gestures
        gestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }


    boolean isExtendedTrackingActive() {
        return extendedTracking;
    }

    final public static int CMD_BACK = -1;
    final public static int CMD_EXTENDED_TRACKING = 1;
    final public static int CMD_AUTOFOCUS = 2;
    final public static int CMD_FLASH = 3;
    final public static int CMD_CAMERA_FRONT = 4;
    final public static int CMD_CAMERA_REAR = 5;
    final public static int CMD_DATASET_START_INDEX = 6;

    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    public void advance() {
        if(chessSet != null) {
            chessSet.advance();
        }
    }

    public void onListen(View view) {
        ProgressBar pb = (ProgressBar) UILayout.findViewById(R.id.progress_sound);
        speechRecognizer = new SpeechRecognizer(this);
        speechRecognizer.addListener(this);
        speechRecognizer.setProgressBar(pb);

        speechRecognizer.startListening();
    }

    @Override
    public void onSpeechResult(IChessComand comand) {
        if(comand != null) {
            //showToast(comand.toString());
            chessSet.execComand(comand);
        } else {
            showToast("Invalid command!");
        }
    }

    @Override
    public ChessPiece onSwitchPiece(List<ChessPiece> pieces) {
        ChessPiece p = null;
        if(pieces.size() == 0) {
            showToast("This piece can't move!");
        } else {
            p = pieces.get(0);
        }

        return p;
    }

    @Override
    public void onPieceMoved(IChessPiece p) {
        if(gameMode == GameMode.MULTPLAYER)
            chessSet.setRotate(true);
        else
            chessSet.setRotate(false);

        chessSet.setCurrentColor(chessSet.getCurrentColor() == PieceColor.BLACK ? PieceColor.WHITE: PieceColor.BLACK);
        if(p != null) {
            showToast("Moving...");
        } else {
            showToast("Returning...");
        }

        speechRecognizer.destroy();
        nextMovie();
    }

    private void nextMovie() {
        if(gameMode == GameMode.MULTPLAYER) {

        } else {
            if(chessSet.getCurrentColor() == PieceColor.BLACK) {

                AsyncMove move = new AsyncMove();
                move.execute(chessSet);

            }
        }
    }

    class AsyncMove extends AsyncTask<ChessSet, Integer, Turn> {
        @Override
        protected Turn doInBackground(ChessSet... params) {
            ChessSet chessSet = params[0];
            //showToast("Other moving...");
            Log.i(LOGTAG, "Calculating...");
            Piece[][] tab = Util.fromPieces(chessSet.getPieces());
            Turn t = Util.getNextTurn(tab, PColor.BLACK, 4);
            Log.i(LOGTAG, "Loops: " + Util.loop + " - " + t.toString());
            Util.loop = 0;
            return t;
        }

        @Override
        protected void onPostExecute(Turn turn) {
            chessSet.execTurn(turn);
        }
    }
* */