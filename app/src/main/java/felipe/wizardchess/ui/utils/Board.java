/*===============================================================================
Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.

Vuforia is a trademark of QUALCOMM Incorporated, registered in the United States 
and other countries. Trademarks of QUALCOMM Incorporated are used with permission.
===============================================================================*/

package felipe.wizardchess.ui.utils;

import java.nio.Buffer;


public class Board extends MeshObject {

    private Buffer mVertBuff;
    private Buffer mTexCoordBuff;
    private Buffer mNormBuff;
    private Buffer mColorBuff;

    private int indicesColor = 0;
    private int verticesNumber = 0;

    private float width = 0.0f;
    private float height = 0.0f;

    public Board(float width, float height) {
        this.width = width;
        this.height = height;
        setVerts();
        setTexCoords();
        setNorms();
        setColor();
    }


    private void setVerts() {
        float scale1 = 0.07f;
        float scale2 = 1.07f;
        float[] TEAPOT_VERTS = {
                -width*scale1, width/16, -height*scale1,
                width*scale2, width/16, -height*scale1,
                -width*scale1, width/16, height*scale2,
                width*scale2, width/16, -height*scale1,
                -width*scale1, width/16, height*scale2,
                width*scale2, width/16, height*scale2
        };

        for(int i = 0; i < TEAPOT_VERTS.length; i++)
            TEAPOT_VERTS[i] -= width/16;

        mVertBuff = fillBuffer(TEAPOT_VERTS);
        verticesNumber = TEAPOT_VERTS.length / 3;
    }


    private void setTexCoords()
    {
        float[] TEAPOT_TEX_COORDS = {
                1, 1,
                0, 1,
                1, 0,
                0, 1,
                1, 0,
                0, 0
        };
        mTexCoordBuff = fillBuffer(TEAPOT_TEX_COORDS);

    }


    private void setNorms()
    {
        float scale1 = 0.04f;
        float scale2 = 1.04f;
        float[] TEAPOT_NORMS = {
                -width*scale1, 1 + width/12, -height*scale1,
                width*scale2, 1 + width/12, -height*scale1,
                -width*scale1, 1 + width/12, height*scale2,
                width*scale2, 1 + width/12, -height*scale1,
                -width*scale1, 1 + width/12, height*scale2,
                width*scale2, 1 + width/12, height*scale2
        };

        for(int i = 0; i < TEAPOT_NORMS.length; i++)
            TEAPOT_NORMS[i] -= width/12;
        mNormBuff = fillBuffer(TEAPOT_NORMS);
    }


    private void setColor()
    {
        float[] TEAPOT_COLOR = {
                1.0f, 1.0f, 1.0f, 1.0f,
                1.0f, 1.0f, 1.0f, 1.0f,
                1.0f, 1.0f, 1.0f, 1.0f,
                1.0f, 1.0f, 1.0f, 1.0f,
                1.0f, 1.0f, 1.0f, 1.0f,
                1.0f, 1.0f, 1.0f, 1.0f
        };
        mColorBuff = fillBuffer(TEAPOT_COLOR);
        indicesColor = TEAPOT_COLOR.length;
    }

    @Override
    public int getNumObjectVertex()
    {
        return verticesNumber;
    }

    @Override
    public int getNumObjectColor() {
        return indicesColor;
    }


    @Override
    public Buffer getBuffer(BUFFER_TYPE bufferType)
    {
        Buffer result = null;
        switch (bufferType)
        {
            case BUFFER_TYPE_VERTEX:
                result = mVertBuff;
                break;
            case BUFFER_TYPE_TEXTURE_COORD:
                result = mTexCoordBuff;
                break;
            case BUFFER_TYPE_NORMALS:
                result = mNormBuff;
                break;
            case BUFFER_TYPE_COLOR:
                result = mColorBuff;
                break;
            default:
                break;

        }

        return result;
    }
    
}
