package felipe.wizardchess.ui.utils;

import android.opengl.Matrix;

import java.nio.Buffer;

import felipe.wizardchess.application.transition.BTransitionCubic;
import felipe.wizardchess.application.transition.BTransitionLinear;
import felipe.wizardchess.application.transition.IBTransition;

/**
 * Created by felipe on 12/07/15.
 */
public class Object3D {
    public static final float SCALE_OBJECT = 1.8f;

    private int advanceCount = 0;

    private Source3D source;
    private int texture;

    private float tx, ty, tz;

    private IBTransition trX, trY, trZ;

    public Object3D(Source3D source, float tx, float ty, float tz) {
        this(source, 0, tx, ty, tz);
    }

    public Object3D(Source3D source, int texture, float tx, float ty, float tz) {
        this.source = source;
        this.texture = texture;

        trX = new BTransitionLinear(200, 0);
        trY = new BTransitionCubic(200, 0, 1, 2);
        //trY = new BTransitionLinear(1000);
        trZ = new BTransitionLinear(200, 0);

        setPosition(tx, ty, tz);
    }

    public void setSource(Source3D source) {
        this.source = source;
    }

    public void setTexture(int texture) {
        this.texture = texture;
    }

    public int getTexture() {
        return texture;
    }

    public void move(float x, float y, float z) {
        trX.addTransition(tx, tx + x);
        trY.addTransition(ty, ty + y);
        trZ.addTransition(tz, tz + z);
    }

    public void setPosition(float x, float y, float z) {
        trX.addTransition(tx, x);
        trY.addTransition(ty, y);
        trZ.addTransition(tz, z);
    }

    public void applyTranslation(float[] matrix) {
        Matrix.translateM(matrix, 0, tx * SCALE_OBJECT, ty * SCALE_OBJECT, tz * SCALE_OBJECT);
    }

    public void unapplyTranslation(float[] matrix) {
        Matrix.translateM(matrix, 0, -tx*SCALE_OBJECT, -ty*SCALE_OBJECT, -tz*SCALE_OBJECT);
    }

    public Buffer getVertices() {
        return source.getVertices();
    }

    public Buffer getNormals() {
        return source.getNormals();
    }

    public Buffer getTexCoords() {
        return source.getTexCoords();
    }

    public Buffer getColorCords() {
        return source.getColorCords();
    }

    public int getNumObjectVertex() {
        return source.getNumObjectVertex();
    }

    public void advance() {
        tx = (float) trX.getNextValue();
        ty = (float) trY.getNextValue();
        tz = (float) trZ.getNextValue();

        /*
        advanceCount++;
        if(advanceCount == 3000) {
            move(1, 1, 1);
            advanceCount = 0;
        }
        */
    }
}
