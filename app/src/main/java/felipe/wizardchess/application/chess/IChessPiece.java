package felipe.wizardchess.application.chess;

import java.util.List;

/**
 * Created by felipe on 12/07/15.
 */
public interface IChessPiece {
    public int getI();
    public int getJ();
    public int getValue();
    public boolean isCaptured();
    public void capture();

    public List<IMoveEvent> getMoveOptions();
    public PieceKey getKey();
    public PieceColor getColor();

    public void setPendingMove(IMoveEvent e);
    public IMoveEvent getPendingMove();
    public boolean move(IMoveEvent e);
    public boolean goBack();

    public boolean inVerticalLineOf(IChessPiece p);
    public boolean inHorizontalLineOf(IChessPiece p);
    public boolean inDiagonalyOf(IChessPiece p);
    public boolean inLOf(IChessPiece p);

}
