package felipe.wizardchess.application.chess;

/**
 * Created by felipe on 13/07/15.
 */
public class ChessComand implements IChessComand {
    String piece;
    int number;
    char letter;
    ComandType type;

    public ChessComand() {
        type = ComandType.COMAND_RETURN;
    }

    public ChessComand(String piece, int number, char letter) {
        this.piece = piece;
        this.number = number;
        this.letter = letter;
        type = ComandType.COMAND_MOVE;
    }

    @Override
    public String getPiece() {
        return piece;
    }

    @Override
    public char getLetter() {
        return letter;
    }

    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public ComandType getType() {
        return type;
    }

    public static IChessComand fromString(String comand) throws WCException {
        ChessComand res = null;

        String[] words = comand.split("\\s+");

        if(words.length != 3 && words.length != 1) {
            throw new WCException(WCException.INVALID_COMMAND);
        }

        if(words.length == 3) {
            try {
                String piece = words[0];
                char letter = words[1].charAt(0);
                int number = Integer.parseInt(words[2]);

                res = new ChessComand(piece, number, letter);
            } catch (Exception e) {
                throw new WCException(WCException.INVALID_COMMAND);
            }
        } else {
            res = new ChessComand();
        }

        return res;
    }

    @Override
    public String toString() {
        return "Comand: " + getPiece() + " " + getLetter() + " " + getNumber();
    }
}
