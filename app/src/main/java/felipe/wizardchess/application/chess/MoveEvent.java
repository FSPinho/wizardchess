package felipe.wizardchess.application.chess;

/**
 * Created by felipe on 12/07/15.
 */
public class MoveEvent implements IMoveEvent {
    IChessPiece thisPiece, nextPiece, passantPiece;
    boolean eating;
    boolean roque = false;

    public MoveEvent(IChessPiece thisPiece, IChessPiece nextPiece, boolean eating, boolean roque) {
        this.thisPiece = thisPiece;
        this.nextPiece = nextPiece;
        this.eating = eating;
        this.roque = roque;
    }

    @Override
    public IChessPiece getThisPiece() {
        return thisPiece;
    }

    @Override
    public void setNextPiece(IChessPiece nextPiece) {
        this.nextPiece = nextPiece;
    }

    @Override
    public IChessPiece getNextPiece() {
        return nextPiece;
    }

    @Override
    public void setPassantPiece(IChessPiece passantPiece) {
        this.passantPiece = passantPiece;
    }

    @Override
    public IChessPiece getPassantPiece() {
        return passantPiece;
    }

    @Override
    public void setEating(boolean eating) {
        this.eating = eating;
    }

    @Override
    public boolean isEating() {
        return eating;
    }

    @Override
    public String toString() {
        return "Move event to (" + getNextPiece().getI() + ", " + getNextPiece().getJ() + ")";
    }

    @Override
    public void execute() {
        thisPiece.move(this);
        if(isEating()) {
            nextPiece.capture();
            if(passantPiece != null)
                passantPiece.capture();
        }
    }

    @Override
    public void goBack() {
        thisPiece.goBack();
        nextPiece.goBack();
        if(passantPiece != null)
            passantPiece.goBack();
    }

    @Override
    public boolean isRoque() {
        return roque;
    }
}
