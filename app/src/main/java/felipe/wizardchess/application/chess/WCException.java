package felipe.wizardchess.application.chess;

/**
 * Created by felipe on 13/07/15.
 */
public class WCException extends Exception {
    public static final String INVALID_COMMAND = "Invalid comand!";

    public WCException(String msg) {
        super(msg);
    }
}
