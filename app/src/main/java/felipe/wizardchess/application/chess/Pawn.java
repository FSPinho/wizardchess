package felipe.wizardchess.application.chess;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import felipe.wizardchess.ui.utils.Source3D;

/**
 * Created by felipe on 12/07/15.
 */
public class Pawn extends ChessPiece {
    public Pawn(Source3D source, int texture, int i, int j, PieceColor color) {
        super(source, texture, i, j, PieceKey.PAWN, color, 1);
    }

    @Override
    public List<IMoveEvent> getMoveOptions() {
        Log.i("CHESS_SET", "Generating moves for PAWN...");
        List<IMoveEvent> mvs = new ArrayList<>();

        boolean initialMove = positions.size() == 1;

        if(getColor() == PieceColor.WHITE) {
            if (getI() < 7) {
                mvs.add(new MoveEvent(this, new EmptyPiece(getI() + 1, getJ()), false, false));
                if(initialMove)
                    mvs.add(new MoveEvent(this, new EmptyPiece(getI() + 2, getJ()), false, false));

                if(getJ() > 0)
                    mvs.add(new MoveEvent(this, new EmptyPiece(getI() + 1, getJ() - 1), false, false));

                if(getJ() < 7)
                    mvs.add(new MoveEvent(this, new EmptyPiece(getI() + 1, getJ() + 1), false, false));
            }
        } else {
            if (getI() > 0) {
                mvs.add(new MoveEvent(this, new EmptyPiece(getI() - 1, getJ()), false, false));
                if(initialMove)
                    mvs.add(new MoveEvent(this, new EmptyPiece(getI() - 2, getJ()), false, false));

                if(getJ() > 0)
                    mvs.add(new MoveEvent(this, new EmptyPiece(getI() - 1, getJ() - 1), false, false));

                if(getJ() < 7)
                    mvs.add(new MoveEvent(this, new EmptyPiece(getI() - 1, getJ() + 1), false, false));
            }
        }

        return mvs;
    }
}
