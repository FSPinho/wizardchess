package felipe.wizardchess.application.chess;

import java.util.ArrayList;
import java.util.List;

import felipe.wizardchess.ui.utils.Source3D;

/**
 * Created by felipe on 12/07/15.
 */
public class Queen extends ChessPiece {

    public Queen(Source3D source, int texture, int i, int j, PieceColor color) {
        super(source, texture, i, j, PieceKey.QUEEN, color, 9);
    }

    @Override
    public List<IMoveEvent> getMoveOptions() {
        List<IMoveEvent> moves = new ArrayList<>();
        for(int i = 0; i < 8; i++)
            for(int j = 0; j < 8; j++) {
                if(!(i == getI() && j == getJ())) {
                    ChessPiece p = new EmptyPiece(i, j);
                    if(inHorizontalLineOf(p) || inVerticalLineOf(p) || inDiagonalyOf(p)) {
                        moves.add(new MoveEvent(this, p, false, false));
                    }
                }
            }

        return moves;
    }

}
