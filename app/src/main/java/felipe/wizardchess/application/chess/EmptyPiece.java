package felipe.wizardchess.application.chess;

import java.util.List;

/**
 * Created by felipe on 13/07/15.
 */
public class EmptyPiece extends ChessPiece {
    public EmptyPiece(int i, int j) {
        super(null, 0, i, j, PieceKey.NONE, null, 0);
    }

    @Override
    public List<IMoveEvent> getMoveOptions() {
        return null;
    }

}
