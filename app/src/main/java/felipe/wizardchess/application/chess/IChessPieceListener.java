package felipe.wizardchess.application.chess;

import java.util.List;

/**
 * Created by felipe on 13/07/15.
 */
public interface IChessPieceListener {
    public ChessPiece onSwitchPiece(List<ChessPiece> pieces);
    public void onPieceMoved(IChessPiece p);
}
