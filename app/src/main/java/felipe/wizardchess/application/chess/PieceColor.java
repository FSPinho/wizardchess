package felipe.wizardchess.application.chess;

/**
 * Created by felipe on 12/07/15.
 */
public enum PieceColor {
    BLACK, WHITE
}
