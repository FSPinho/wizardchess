package felipe.wizardchess.application.chess;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import felipe.wizardchess.application.algorithm.PColor;
import felipe.wizardchess.application.algorithm.Turn;
import felipe.wizardchess.application.algorithm.Util;
import felipe.wizardchess.application.transition.BTransition;
import felipe.wizardchess.application.transition.BTransitionExpoIn;
import felipe.wizardchess.ui.utils.Board;
import felipe.wizardchess.ui.utils.Object3D;
import felipe.wizardchess.ui.utils.Source3D;
import felipe.wizardchess.ui.common.TextureHelper;

/**
 * Created by felipe on 11/07/15.
 */
public class ChessSet {
    private static final String LOGTAG = "Chess Set";
    private static final float ANGLE_BLACK = 0.0f;
    private static final float ANGLE_WHITE = 180.0f;
    Context context;

    private int textureB, textureW;
    private List<ChessPiece> pieces;
    private List<IMoveEvent> executedMoves;
    private Board board;

    private PieceColor currentColor = PieceColor.WHITE;

    private IChessPieceListener listener = null;

    private BTransition angleTransition;
    private float boardAngle = ANGLE_WHITE;
    private boolean rotate = true;

    public ChessSet(Context context) {
        this.context = context;
        executedMoves = new Vector<>();

        angleTransition = new BTransitionExpoIn(200, 300);
        setCurrentColor(PieceColor.WHITE);

        pieces = new Vector<>();

        synchronized (pieces) {

            pieces.add(new King(null, textureB, 7, 3, PieceColor.BLACK));
            pieces.add(new Queen(null, textureB, 7, 4, PieceColor.BLACK));
            pieces.add(new Rook(null, textureB, 7, 0, PieceColor.BLACK));
            pieces.add(new Rook(null, textureB, 7, 7, PieceColor.BLACK));
            pieces.add(new Bishop(null, textureB, 7, 2, PieceColor.BLACK));
            pieces.add(new Bishop(null, textureB, 7, 5, PieceColor.BLACK));
            pieces.add(new Knight(null, textureB, 7, 1, PieceColor.BLACK));
            pieces.add(new Knight(null, textureB, 7, 6, PieceColor.BLACK));
            pieces.add(new Pawn(null, textureB, 6, 0, PieceColor.BLACK));
            pieces.add(new Pawn(null, textureB, 6, 1, PieceColor.BLACK));
            pieces.add(new Pawn(null, textureB, 6, 2, PieceColor.BLACK));
            pieces.add(new Pawn(null, textureB, 6, 3, PieceColor.BLACK));
            pieces.add(new Pawn(null, textureB, 6, 4, PieceColor.BLACK));
            pieces.add(new Pawn(null, textureB, 6, 5, PieceColor.BLACK));
            pieces.add(new Pawn(null, textureB, 6, 6, PieceColor.BLACK));
            pieces.add(new Pawn(null, textureB, 6, 7, PieceColor.BLACK));

                /*
                   0 1 2 3 4 5 6 7
                 7 R H B Q K B H R
                 6 P P P P P P P P
                 5
                 4
                 3
                 2
                 1 R H B Q K B H R
                 0 P P P P P P P P
                   7 6 5 4 3 2 1 0

                 */

            pieces.add(new King(null, textureW, 0, 3, PieceColor.WHITE));
            pieces.add(new Queen(null, textureW, 0, 4, PieceColor.WHITE));
            pieces.add(new Rook(null, textureW, 0, 0, PieceColor.WHITE));
            pieces.add(new Rook(null, textureW, 0, 7, PieceColor.WHITE));
            pieces.add(new Bishop(null, textureW, 0, 2, PieceColor.WHITE));
            pieces.add(new Bishop(null, textureW, 0, 5, PieceColor.WHITE));
            pieces.add(new Knight(null, textureW, 0, 1, PieceColor.WHITE));
            pieces.add(new Knight(null, textureW, 0, 6, PieceColor.WHITE));
            pieces.add(new Pawn(null, textureW, 1, 0, PieceColor.WHITE));
            pieces.add(new Pawn(null, textureW, 1, 1, PieceColor.WHITE));
            pieces.add(new Pawn(null, textureW, 1, 2, PieceColor.WHITE));
            pieces.add(new Pawn(null, textureW, 1, 3, PieceColor.WHITE));
            pieces.add(new Pawn(null, textureW, 1, 4, PieceColor.WHITE));
            pieces.add(new Pawn(null, textureW, 1, 5, PieceColor.WHITE));
            pieces.add(new Pawn(null, textureW, 1, 6, PieceColor.WHITE));
            pieces.add(new Pawn(null, textureW, 1, 7, PieceColor.WHITE));
        }
    }

    public void setSwitchListener(IChessPieceListener listener) {
        this.listener = listener;
    }

    public void setRotate(boolean rotate) {
        this.rotate = rotate;
    }

    public boolean isRotate() {
        return rotate;
    }

    public synchronized void loadModels() {
        board = new Board(8 * Object3D.SCALE_OBJECT, 8 * Object3D.SCALE_OBJECT);

        try {
            Source3D king = new Source3D();
            king.loadModel(context.getResources().getAssets(), "ChessPieces/rei.txt");
            Source3D queen = new Source3D();
            queen.loadModel(context.getResources().getAssets(), "ChessPieces/rainha.txt");
            Source3D rook = new Source3D();
            rook.loadModel(context.getResources().getAssets(), "ChessPieces/torre.txt");
            Source3D bishop = new Source3D();
            bishop.loadModel(context.getResources().getAssets(), "ChessPieces/bispo.txt");
            Source3D knight = new Source3D();
            knight.loadModel(context.getResources().getAssets(), "ChessPieces/cavalo.txt");
            Source3D pawn = new Source3D();
            pawn.loadModel(context.getResources().getAssets(), "ChessPieces/peao.txt");

            for(ChessPiece p: pieces) {
                switch (p.getKey()) {
                    case KING:
                        p.setSource(king);
                        break;
                    case QUEEN:
                        p.setSource(queen);
                        break;
                    case BISHOP:
                        p.setSource(bishop);
                        break;
                    case KNIGHT:
                        p.setSource(knight);
                        break;
                    case ROOK:
                        p.setSource(rook);
                        break;
                    case PAWN:
                        p.setSource(pawn);
                        break;
                }
            }

        } catch (IOException e) {}
    }

    public float getBoardAngle() {
        return boardAngle;
    }

    public Board getBoard() {
        return board;
    }

    public void setCurrentColor(PieceColor currentColor) {
        this.currentColor = currentColor;

        if (rotate) {
            if (currentColor == PieceColor.BLACK) {
                angleTransition.addTransition(boardAngle, ANGLE_BLACK);
            } else {
                angleTransition.addTransition(boardAngle, ANGLE_WHITE);
            }
        }
    }

    public PieceColor getCurrentColor() {
        return currentColor;
    }

    public void setTextures(int textureBID, int textureWID) {
        textureB = TextureHelper.loadTexture(context, textureBID);
        textureW = TextureHelper.loadTexture(context, textureWID);
    }

    public List<ChessPiece> getPieces() {
        return pieces;
    }

    public int getTextureB() {
        return textureB;
    }

    public int getTextureW() {
        return textureW;
    }

    private ChessPiece getPiece(int i, int j) {
        ChessPiece piece = null;

        for(ChessPiece p: pieces) {
            if(p.getI() == i && p.getJ() == j && !p.isCaptured()) {
                piece = p;
                break;
            }
        }

        return piece;
    }

    private List<IMoveEvent> getCleanedMoves(ChessPiece piece) {
        //Log.i(LOGTAG, "Gettin cleaned");
        List<IMoveEvent> events = piece.getMoveOptions();
        List<IMoveEvent> cleaned = new ArrayList<>();
        if(events != null) {
            for(IMoveEvent e: events) {
                IChessPiece next = getPiece(e.getNextPiece().getI(), e.getNextPiece().getJ());
                if(next == null)
                    next = e.getNextPiece();
                else {
                    e.setNextPiece(next);
                    e.setEating(true);
                    //Log.i(LOGTAG, "Setting eating true");
                }

                IChessPiece thisPiece = e.getThisPiece();

                switch (e.getThisPiece().getKey()) {
                    case PAWN: {
                        if(thisPiece.inDiagonalyOf(next) && e.isEating() && thisPiece.getColor() != next.getColor()) {
                            cleaned.add(e);
                            //Log.i(LOGTAG, "Pawn eating");
                        }
                        else if(thisPiece.inVerticalLineOf(next) && next.getKey() == PieceKey.NONE) {
                            cleaned.add(e);
                            //Log.i(LOGTAG, "Pawn in vertical: this " + thisPiece + ", next " + next);
                        }

                        if(thisPiece.inDiagonalyOf(next)) {
                            // If passant
                            if (thisPiece.getColor() == PieceColor.WHITE && thisPiece.getI() == 4) {
                                ChessPiece left = getPiece(thisPiece.getI(), thisPiece.getJ() - 1);
                                ChessPiece right = getPiece(thisPiece.getI(), thisPiece.getJ() + 1);

                                if (left != null && left.isSecondMove()) {
                                    IMoveEvent event = new MoveEvent(thisPiece, next, true, false);
                                    event.setPassantPiece(left);
                                    cleaned.add(e);
                                    //Log.i(LOGTAG, "Pawn in vertical: this " + thisPiece + ", next " + next);
                                } else if (right != null && right.isSecondMove()) {
                                    IMoveEvent event = new MoveEvent(thisPiece, next, true, false);
                                    event.setPassantPiece(right);
                                    cleaned.add(e);
                                }
                            } else if (thisPiece.getColor() == PieceColor.BLACK && thisPiece.getI() == 3) {
                                ChessPiece left = getPiece(thisPiece.getI(), thisPiece.getJ() - 1);
                                ChessPiece right = getPiece(thisPiece.getI(), thisPiece.getJ() + 1);

                                if (left != null && left.isSecondMove()) {
                                    IMoveEvent event = new MoveEvent(thisPiece, next, true, false);
                                    event.setPassantPiece(left);
                                    cleaned.add(e);
                                } else if (right != null && right.isSecondMove()) {
                                    IMoveEvent event = new MoveEvent(thisPiece, next, true, false);
                                    event.setPassantPiece(right);
                                    cleaned.add(e);
                                }
                            }
                        }

                        break;
                    } case ROOK: {
                        if(thisPiece.inVerticalLineOf(next) || thisPiece.inHorizontalLineOf(next)) {
                            if(next.getKey() == PieceKey.NONE || (next.getColor() != thisPiece.getColor()))
                                cleaned.add(e);
                        }
                        break;
                    } case KNIGHT: {
                        if(thisPiece.inLOf(next)) {
                            if(next.getKey() == PieceKey.NONE || next.getColor() != thisPiece.getColor())
                                cleaned.add(e);
                        }
                        break;
                    } case BISHOP: {
                        if(thisPiece.inDiagonalyOf(next)) {
                            if(next.getKey() == PieceKey.NONE || next.getColor() != thisPiece.getColor())
                                cleaned.add(e);
                        }
                        break;
                    } case QUEEN: {
                        if(thisPiece.inVerticalLineOf(next) || thisPiece.inHorizontalLineOf(next) || thisPiece.inDiagonalyOf(next)) {
                            if(next.getKey() == PieceKey.NONE || (next.getColor() != thisPiece.getColor()))
                                cleaned.add(e);
                        }
                        break;
                    } case KING: {
                        if(thisPiece.inVerticalLineOf(next) || thisPiece.inHorizontalLineOf(next) || thisPiece.inDiagonalyOf(next)) {
                            if(next.getKey() == PieceKey.NONE || (next.getColor() != thisPiece.getColor()))
                                cleaned.add(e);
                        }
                        break;
                    }

                }

            }
        }

        return cleaned;
    }

    private List<ChessPiece> getPieces(PieceKey key, PieceColor color, int nextI, int nextJ) {
        List<ChessPiece> sp = new ArrayList<>();

        Log.i(LOGTAG, "Verify: " + key + ", " + color);
        for(ChessPiece p: pieces) {
            if(p.getKey() == key && p.getColor() == color && !p.isCaptured()) {
                List<IMoveEvent> events = getCleanedMoves(p);

                for(IMoveEvent e: events) {
                    Log.i(LOGTAG, e.toString());
                    if(e.getNextPiece().getI() == nextI && e.getNextPiece().getJ() == nextJ) {
                        sp.add(p);
                        break;
                    }
                }
            }
        }

        return sp;
    }

    public void execTurn(Turn turn) {
        IChessPiece p = makeTurn(turn, false);

        if (listener != null) {
            listener.onPieceMoved(p);
        }
    }

    private IChessPiece makeTurn(Turn turn, boolean roque) {
        IChessPiece next = getPiece(turn.ni, turn.nj);
        IChessPiece tPiece = getPiece(turn.ti, turn.tj);
        IMoveEvent e = new MoveEvent(tPiece, next==null? new EmptyPiece(turn.ni, turn.nj): next, next != null, roque);
        executedMoves.add(0, e);
        e.execute();

        return e.getThisPiece();
    }

    private King getKing(PieceColor color) {
        for(ChessPiece p: pieces) {
            if(p.getKey() == PieceKey.KING && p.getColor() == color)
                return (King) p;
        }

        return null;
    }

    private IMoveEvent getPassantMoveTo(PieceKey key, PieceColor color, int i, int j) {
        ChessPiece thisPiece = null;
        List<ChessPiece> thisPieces = getPieces(key, color, i, j);
        if(thisPieces.size() >= 1)
            thisPiece = thisPieces.get(0);

        if(thisPiece != null) {
            // Passant
            if (thisPiece.getColor() == PieceColor.WHITE && thisPiece.getI() == 4) {
                ChessPiece left = getPiece(thisPiece.getI(), thisPiece.getJ() - 1);
                ChessPiece right = getPiece(thisPiece.getI(), thisPiece.getJ() + 1);

                if (left != null && left.isSecondMove()) {
                    IMoveEvent event = new MoveEvent(thisPiece, getPiece(i, j), true, false);
                    event.setPassantPiece(left);
                    return event;
                } else if (right != null && right.isSecondMove()) {
                    IMoveEvent event = new MoveEvent(thisPiece, getPiece(i, j), true, false);
                    event.setPassantPiece(right);
                    return event;
                }
            } else if (thisPiece.getColor() == PieceColor.BLACK && thisPiece.getI() == 3) {
                ChessPiece left = getPiece(thisPiece.getI(), thisPiece.getJ() - 1);
                ChessPiece right = getPiece(thisPiece.getI(), thisPiece.getJ() + 1);

                if (left != null && left.isSecondMove()) {
                    IMoveEvent event = new MoveEvent(thisPiece, getPiece(i, j), true, false);
                    event.setPassantPiece(left);
                    return event;
                } else if (right != null && right.isSecondMove()) {
                    IMoveEvent event = new MoveEvent(thisPiece, getPiece(i, j), true, false);
                    event.setPassantPiece(right);
                    return event;
                }
            }
        }
        return null;
    }

    public IMoveEvent execComand(IChessComand comand) {
        IMoveEvent e = null;
        if(comand.getType() == IChessComand.ComandType.COMAND_MOVE) {

            // This Piece
            PieceKey key = PieceKey.parse(comand.getPiece());
            PieceColor color = getCurrentColor();
            int i = (comand.getNumber() - 1);
            int j = 7 - (Character.getNumericValue(comand.getLetter()) - Character.getNumericValue('a'));

            // If Roque
            if(key == PieceKey.KING && (j == 1 || j == 5)) {
                List<Turn> turns = Util.getTurnsToRoque(Util.fromPieces(pieces), color == PieceColor.BLACK ? PColor.BLACK : PColor.WHITE);

                if(turns.size() >= 4) {
                    Turn t1 = turns.get(0);
                    Turn t2 = turns.get(1);

                    Turn t3 = turns.get(2);
                    Turn t4 = turns.get(3);

                    if(t1.nj == j && getPiece(t1.ti, t1.tj).isInitialMove() && getPiece(t2.ti, t2.tj).isInitialMove()) {
                        makeTurn(t1, false);
                        makeTurn(t2, true);
                    } else if (t3.nj == j && getPiece(t3.ti, t3.tj).isInitialMove() && getPiece(t4.ti, t4.tj).isInitialMove()) {
                        makeTurn(t3, false);
                        makeTurn(t4, true);
                    }
                } else if(turns.size() >= 2) {
                    Turn t1 = turns.get(0);
                    Turn t2 = turns.get(1);

                    if(t1.nj == j && getPiece(t1.ti, t1.tj).isInitialMove() && getPiece(t2.ti, t2.tj).isInitialMove()) {
                        makeTurn(t1, false);
                        makeTurn(t2, true);
                    }
                }

            } else {

                List<ChessPiece> thisPieces = getPieces(key, color, i, j);

                ChessPiece thisPiece = null;

                IMoveEvent pEvent = getPassantMoveTo(key, color, i, j);
                // If Passant
                if(pEvent != null) {
                    e.execute();

                } else {
                    if (thisPieces.size() > 1) {
                        if (listener != null)
                            thisPiece = listener.onSwitchPiece(thisPieces);
                    } else if (thisPieces.size() == 1) {
                        thisPiece = thisPieces.get(0);
                    }

                    if (thisPiece != null) {
                        ChessPiece nextPiece = getPiece(i, j);
                        e = null;
                        if (nextPiece == null) {
                            nextPiece = new EmptyPiece(i, j);
                            e = new MoveEvent(thisPiece, nextPiece, false, false);
                        } else {
                            e = new MoveEvent(thisPiece, nextPiece, true, false);
                        }

                        executedMoves.add(0, e);
                        e.execute();
                    } else {
                        return null;
                    }
                }
            }

            if (listener != null) {
                if (e != null)
                    listener.onPieceMoved(e.getThisPiece());
                else
                    listener.onPieceMoved(null);
            }
        } else {
            returnMove();
            returnMove();
        }

        return e;
    }

    private void returnMove() {
        if(executedMoves.size() > 0) {
            if(executedMoves.get(0).isRoque()) {
                executedMoves.get(0).goBack();
                executedMoves.remove(0);

                executedMoves.get(0).goBack();
                executedMoves.remove(0);
            } else {
                executedMoves.get(0).goBack();
                executedMoves.remove(0);
            }
        }
    }

    public synchronized void advance() {
        this.boardAngle = (float) angleTransition.getNextValue();

        if (pieces != null)
            synchronized (pieces) {
                for (Object3D obj : pieces)
                    obj.advance();
            }
    }

}
