package felipe.wizardchess.application.chess;

/**
 * Created by felipe on 12/07/15.
 */
public interface IMoveEvent {
    public IChessPiece getThisPiece();
    public void setNextPiece(IChessPiece piece);
    public IChessPiece getNextPiece();

    public void setPassantPiece(IChessPiece piece);
    public IChessPiece getPassantPiece();

    public void setEating(boolean eating);
    public boolean isEating();

    public void execute();
    public void goBack();

    public boolean isRoque();
}
