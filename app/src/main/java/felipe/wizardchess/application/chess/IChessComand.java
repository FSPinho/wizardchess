package felipe.wizardchess.application.chess;

/**
 * Created by felipe on 13/07/15.
 */
public interface IChessComand {
    enum ComandType {COMAND_RETURN, COMAND_MOVE, COMAND_ROCK};

    public String getPiece();
    public char getLetter();
    public int getNumber();
    public ComandType getType();
}
