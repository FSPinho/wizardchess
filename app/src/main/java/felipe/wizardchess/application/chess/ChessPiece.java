package felipe.wizardchess.application.chess;

import java.util.List;
import java.util.Vector;

import felipe.wizardchess.ui.utils.Object3D;
import felipe.wizardchess.ui.utils.Source3D;

/**
 * Created by felipe on 12/07/15.
 */
public abstract class ChessPiece extends Object3D implements IChessPiece {
    public static int ID_COUNT = 0;

    private int id;

    private int i, j;
    private int defI, defJ;

    private int value;
    private boolean captured = false;
    private PieceKey key;
    private PieceColor color;
    private IMoveEvent pendingMove;

    protected List<PiecePosition> positions;

    public ChessPiece(Source3D source, int texture, int i, int j, PieceKey key, PieceColor color, int value) {
        super(source, texture, j, 0, i);

        this.id = ID_COUNT++;

        this.key = key;
        this.color= color;
        this.value = value;
        this.i = i;
        this.j = j;

        if(i < 4)
            defI = i - 3;
        else
            defI = i + 3;

        defJ = j;

        positions = new Vector<>();
        positions.add(0, new PiecePosition(i, j));
    }

    @Override
    public int getI() {
        return i;
    }

    @Override
    public int getJ() {
        return j;
    }

    public void setDefI(int defI) {
        this.defI = defI;
    }

    public void setDefJ(int defJ) {
        this.defJ = defJ;
    }

    @Override
    public boolean move(IMoveEvent e) {
        positions.add(0, new PiecePosition(i, j));
        i = e.getNextPiece().getI();
        j = e.getNextPiece().getJ();
        setPosition(j, 0, i);
        return true;
    }

    @Override
    public boolean goBack() {
        captured = false;
        if(positions.size() > 0) {
            i = positions.get(0).getI();
            j = positions.get(0).getJ();
            setPosition(j, 0, i);

            if(positions.size() > 1)
                positions.remove(0);
        }
        return true;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public boolean isCaptured() {
        return captured;
    }

    @Override
    public void capture() {
        captured = true;
        positions.add(0, new PiecePosition(i, j));
        i = defI;
        j = defJ;
        setPosition(j, 0, i);
    }

    @Override
    public PieceKey getKey() {
        return key;
    }

    @Override
    public PieceColor getColor() {
        return color;
    }

    @Override
    public boolean inVerticalLineOf(IChessPiece p) {
        return j == p.getJ();
    }

    @Override
    public boolean inHorizontalLineOf(IChessPiece p) {
        return i == p.getI();
    }

    @Override
    public boolean inDiagonalyOf(IChessPiece p) {
        return Math.abs(i - p.getI()) == Math.abs(j - p.getJ());
    }

    @Override
    public boolean inLOf(IChessPiece p) {
        return (Math.abs(i - p.getI()) == 1 && Math.abs(j - p.getJ()) == 2) ||
                (Math.abs(i - p.getI()) == 2 && Math.abs(j - p.getJ()) == 1);
    }

    @Override
    public void setPendingMove(IMoveEvent pendingMove) {
        this.pendingMove = pendingMove;
    }

    @Override
    public IMoveEvent getPendingMove() {
        return pendingMove;
    }

    @Override
    public String toString() {
        return "Piece(" + i + ", " + j + ")";
    }

    private class PiecePosition {
        int i, j;

        public PiecePosition(int i, int j) {
            this.i = i;
            this.j = j;
        }

        public int getI() {
            return i;
        }

        public int getJ() {
            return j;
        }
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        return id == ((ChessPiece)o).getId();
    }

    public boolean isInitialMove() {
        boolean initialMove = positions.size() == 1;
        return initialMove;
    }

    public boolean isSecondMove() {
        boolean secondlMove = positions.size() == 2;
        return secondlMove;
    }
}
