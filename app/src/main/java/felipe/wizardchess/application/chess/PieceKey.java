package felipe.wizardchess.application.chess;

/**
 * Created by felipe on 12/07/15.
 */
public enum PieceKey {
    KING,
    QUEEN,
    ROOK,
    BISHOP,
    KNIGHT,
    PAWN,
    NONE;

    public static PieceKey parse(String piece) {
        if(piece.equals("king")) {
            return KING;
        } else if(piece.equals("queen")) {
            return QUEEN;
        } else if(piece.equals("rook")) {
            return ROOK;
        } else if(piece.equals("bishop")) {
            return BISHOP;
        } else if(piece.equals("knight")) {
            return KNIGHT;
        } else if(piece.equals("pawn")) {
            return PAWN;
        }

        return null;
    }

}
