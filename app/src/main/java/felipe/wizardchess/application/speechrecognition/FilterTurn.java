package felipe.wizardchess.application.speechrecognition;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by felipe on 12/07/15.
 */
public class FilterTurn {
    private static final int POINTS_FOR_EQUALS = 100;
    private static final int POINTS_FOR_PAIR_SEQ = 10;
    private static final int POINTS_FOR_LETTER = 1;
    private static final String[] RETURN = new String[] {"return", "reurn", "retun", "retorn", "retorne", "goback", "back", "go back", "undo", "voltar", "volte", "desfazer", "desfaça"};
    private static final String[] KING = new String[] {"king", "kim", "kings", "quinn", "rei", "hey", "reis"};
    private static final String[] QUEEN = new String[] {"queen", "queens", "keen", "rainha", "rain", "rainhas"};
    private static final String[] BISHOP = new String[] {"bishop", "bishop", "bestshop", "bisharp", "bispo", "bispos"};
    private static final String[] KNIGHT = new String[] {"knight", "knight", "tonight", "night", "cavalo", "cavalos", "horse", "horns", "rogers", "ross"};
    private static final String[] ROOK = new String[] {"rook", "rock", "roque", "hulk", "huck", "torre", "torres", "torry", "torrent"};
    private static final String[] PAWN = new String[] {"pawn", "porn", "pono", "paulo", "peão", "pião", "tião", "leão", "piam"};

    private static final String[] A = {"a", "ei", "ai", "à", "á", "há", "ah", "ra", "ar", "air", "avião", "ave", "arma", "amor", "abacate", "armário"};
    private static final String[] B = {"b", "be", "bi", "bb", "bola", "banana", "boneca", "batata"};
    private static final String[] C = {"c", "cc", "cê", "ce", "ser", "sê", "se", "casa", "carro", "cão", "cachorro", "cenoura", "cebola"};
    private static final String[] D = {"d", "the", "de", "der", "dê", "die", "diy", "gui", "ig", "gif", "dado", "doce", "dia"};
    private static final String[] E = {"e", "i", "é", "air", "eh", "ir", "ê", "elefante", "escada", "escola", "eva", "elefante"};
    private static final String[] F = {"f", "efe", "ef", "s", "ff", "esse", "fogo", "faca", "formiga", "física"};
    private static final String[] G = {"g", "dj", "jazz", "j", "gato", "gol", "gola", "galo", "galinha", "garfo", "farrafa"};
    private static final String[] H = {"h", "aga", "eight", "hospital"};

    private static final String[] ONE = {"1", "one", "um"};
    private static final String[] TWO = {"two", "2", "dois", "too"};
    private static final String[] THREE = {"three", "tree", "três", "treis", "3"};
    private static final String[] FOUR = {"4", "four", "for", "quantro", "quarto"};
    private static final String[] FIVE = {"five", "live", "5", "cinco", "cinto", "sinto"};
    private static final String[] SIX = {"seis", "6", "six", "sex", "sexo", "sexy", "sexta", "sim"};
    private static final String[] SEVEN = {"seven", "7", "sete", "set"};
    private static final String[] EIGHT = {"eight", "8", "oito", "eita", "face"};

    public CleanedResult clean(List<String> text) {
        CleanedResult res = new CleanedResult(0, "");
        int score = 0;

        for(String s: text) {
            CleanedResult tmp = clean(s);
            if(tmp != null && tmp.getValue() > score) {
                score = tmp.getValue();
                res = tmp;
            }

        }

        return res;
    }

    public CleanedResult clean(String text) {
        CleanedResult result = null;
        String[] words = text.split("\\s+");

        if(words.length < 1) {
            result = new CleanedResult(0, "");
        } else {
            String res = "";
            int score = 0;

            CleanedResult tmp = cleanCommand(words[0]);

            if(tmp.getValue() > 0) {

                res += tmp.getText();
                score += tmp.getValue();
                if(words.length == 1 && tmp.getText() == "return") {
                    result = new CleanedResult(score, res);

                } if (words.length >= 3) {
                    CleanedResult tmp2 = cleanCommand(words[1] + " " + words[2]);
                    res += " " + tmp2.getText();
                    score += tmp2.getValue();
                    result = new CleanedResult(score, res);
                }

            } else {
                result = new CleanedResult(0, "");
            }
        }

        return result;
    }

    private CleanedResult cleanCommand(String text) {
        CleanedResult result = null;

        text = text.toLowerCase();
        String[] words = text.split("\\s+");

        if(words.length == 1) {

            int maior = 0;
            String piece = "";

            int returns = getPointsTo(RETURN, words[0]);
            if(returns > maior) { maior = returns; piece = "return"; }

            int king = getPointsTo(KING, words[0]);
            if(king > maior) { maior = king; piece = "king"; }

            int queen = getPointsTo(QUEEN, words[0]);
            if(queen > maior) { maior = queen; piece = "queen"; }

            int bishop = getPointsTo(BISHOP, words[0]);
            if(bishop > maior) { maior = bishop; piece = "bishop"; }

            int knight = getPointsTo(KNIGHT, words[0]);
            if(knight > maior) { maior = knight; piece = "knight"; }

            int rook = getPointsTo(ROOK, words[0]);
            if(rook > maior) { maior = rook; piece = "rook"; }

            int pawn = getPointsTo(PAWN, words[0]);
            if(pawn > maior) { maior = pawn; piece = "pawn"; }

            result = new CleanedResult(maior, piece);

        } else if(words.length >= 2) {
            int score = 0;
            int maior = 0;

            String letter = "";
            int a = getPointsToLetter("a", words[0]);
            if(a > maior) { maior = a; letter = "a"; }

            int b = getPointsToLetter("b", words[0]);
            if(b > maior) { maior = b; letter = "b"; }

            int c = getPointsToLetter("c", words[0]);
            if(c > maior) { maior = c; letter = "c"; }

            int d = getPointsToLetter("d", words[0]);
            if(d > maior) { maior = d; letter = "d"; }

            int e = getPointsToLetter("e", words[0]);
            if(e > maior) { maior = e; letter = "e"; }

            int f = getPointsToLetter("f", words[0]);
            if(f > maior) { maior = f; letter = "f"; }

            int g = getPointsToLetter("g", words[0]);
            if(g > maior) { maior = g; letter = "g"; }

            int h = getPointsToLetter("h", words[0]);
            if(h > maior) { maior = h; letter = "h"; }

            score += maior;
            maior = 0;
            String number = "";
            int one = getPointsTo(ONE, words[1]);
            if(one > maior) { maior = one; number = "1"; }

            int two = getPointsTo(TWO, words[1]);
            if(two > maior) { maior = two; number = "2"; }

            int three = getPointsTo(THREE, words[1]);
            if(three > maior) { maior = three; number = "3"; }

            int four = getPointsTo(FOUR, words[1]);
            if(four > maior) { maior = four; number = "4"; }

            int five = getPointsTo(FIVE, words[1]);
            if(five > maior) { maior = five; number = "5"; }

            int six = getPointsTo(SIX, words[1]);
            if(six > maior) { maior = six; number = "6"; }

            int seven = getPointsTo(SEVEN, words[1]);
            if(seven > maior) { maior = seven; number = "7"; }

            int eight = getPointsTo(EIGHT, words[1]);
            if(eight > maior) { maior = eight; number = "8"; }

            score += maior;

            String res = letter + " " + number;

            result = new CleanedResult(score, res);
        }

        return result;
    }

    private int getPointsToLetter(String letter, String text) {
        if(letter.charAt(0) == text.charAt(0))
            return 100;
        return 0;
    }

    private int getPointsTo(String[] patern, String text) {
        text = text.toLowerCase();
        int score = 0;

        List<Integer> points = new ArrayList<>();
        for(String s: patern) {
            int p = 0;
            if(s.equals(text))
                p += POINTS_FOR_EQUALS;
            points.add(p);
        }

        for(Integer i: points)
            if((int)i > score)
                score = i;

        return score;
    }

}
