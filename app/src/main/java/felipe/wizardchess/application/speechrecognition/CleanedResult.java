package felipe.wizardchess.application.speechrecognition;

/**
 * Created by felipe on 12/07/15.
 */
public class CleanedResult {
    int value;
    String text;

    public CleanedResult(int value, String text) {
        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return text + ": " + value;
    }
}
