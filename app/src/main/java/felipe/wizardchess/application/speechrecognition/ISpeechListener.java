package felipe.wizardchess.application.speechrecognition;

import felipe.wizardchess.application.chess.IChessComand;

/**
 * Created by felipe on 13/07/15.
 */
public interface ISpeechListener {
    public void onSpeechResult(IChessComand comand);
}
