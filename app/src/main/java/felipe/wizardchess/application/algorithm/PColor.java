package felipe.wizardchess.application.algorithm;

public enum PColor {
	BLACK, 
	WHITE, 
	NONE;
	
	public PColor getInverse() {
		return this == BLACK? WHITE: this == WHITE? BLACK: NONE;
	}
	
	@Override
	public String toString() {
		if(this == BLACK) return "B";
		if(this == WHITE) return "W";
		return "_";
	}
}
