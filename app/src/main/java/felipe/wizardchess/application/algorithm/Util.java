package felipe.wizardchess.application.algorithm;

import java.util.ArrayList;
import java.util.List;

import felipe.wizardchess.application.chess.ChessPiece;
import felipe.wizardchess.application.chess.IChessPiece;

public class Util {
	public static int loop = 0;

	public static Turn getNextTurn(Piece[][] tab, PColor color, int level) {
		List<Turn> bests = new ArrayList<>();
		Turn t = getMax(tab, color, level, level, bests);
		if(t.value == 0) {
			System.out.println(bests);
			int i = (int) (Math.random() * bests.size());
			return bests.get(i);
		}

		return t;
	}

	public static Turn getMax(Piece[][] tab, PColor color, int level, int baseLevel, List<Turn> bests) {
		loop++;
		
		if(level == 0) return null;
		List<Turn> turns = getTurnsFor(tab, color);
		
		if(turns.size() == 0)
			return null;
		
		Turn tMax = null;
		int max = -9999999;
		
		for(Turn t: turns) {
			t.makeTurn(tab);
			Turn aux = getMax(tab, color.getInverse(), level - 1, baseLevel, bests);
			
			if(aux != null) {
				int newMax = (t.value + aux.min) - aux.value;				
				if(newMax > max) {
					tMax = t;
					tMax.value = newMax;
					tMax.min = aux.value;
					max = newMax;
				}
			} else {
				int newMax = t.value;
				if(newMax > max) {
					tMax = t;
					tMax.value = newMax;
					tMax.min = 0;
					max = newMax;
				}
			}
			
			t.goBack(tab);
			
		}

		if(level == baseLevel && tMax != null && tMax.value >= 0)
			bests.add(tMax);

		return tMax;
	}
	
	private static List<Turn> getTurnsFor(Piece[][] tab, PColor c) {
		List<Turn> turns = new ArrayList<Turn>();
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				Piece piece = tab[i][j];
				if(piece.color == c) {
					switch(piece.key) {
						case KING:
							turns.addAll(getForKing(tab, i, j));
							break;
						case QUEEN:
							turns.addAll(getForQueen(tab, i, j));
							break;
						case BISHOP:
							turns.addAll(getForBishoe(tab, i, j));
							break;
						case KNIGHT:
							turns.addAll(getForKnight(tab, i, j));
							break;
						case ROOK:
							turns.addAll(getForRook(tab, i, j));
							break;
						case PAWN:
							turns.addAll(getForPawn(tab, i, j));
							break;
					}
				}
			}
		}
		
		return turns;
	}
	
	private static List<Turn> getForPawn(Piece[][] tab, int i, int j) {
		List<Turn> turns = new ArrayList<Turn>();
		int a, b;
		
		if(tab[i][j].color == PColor.WHITE) {
			a = i+1;
			b = j;
			if(isValid(a, b) && tab[a][b].key == PKey.NONE) {
				turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
			}
			
			a = i+1;
			b = j+1;
			if(isValid(a, b) && tab[a][b].color != tab[i][j].color && tab[a][b].key != PKey.NONE) {
				turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
			}
			
			a = i+1;
			b = j-1;
			if(isValid(a, b) && tab[a][b].color != tab[i][j].color && tab[a][b].key != PKey.NONE) {
				turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
			}
		} else if(tab[i][j].color == PColor.BLACK) {
			a = i-1;
			b = j;
			if(isValid(a, b) && tab[a][b].key == PKey.NONE) {
				turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
			}
			
			a = i-1;
			b = j+1;
			if(isValid(a, b) && tab[a][b].color != tab[i][j].color && tab[a][b].key != PKey.NONE) {
				turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
			}
			
			a = i-1;
			b = j-1;
			if(isValid(a, b) && tab[a][b].color != tab[i][j].color && tab[a][b].key != PKey.NONE) {
				turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
			}
		}
		
		return turns;
	}
	
	private static List<Turn> getForKing(Piece[][] tab, int i, int j) {
		List<Turn> turns = new ArrayList<Turn>();
		for(int a = i - 1; a < i + 2; a++) {
			for(int b = j - 1; b < j + 2; b++) {
				if(!(a == i && b == j) && isValid(a, b)) {
					if(tab[a][b].color != tab[i][j].color)
						turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
				}
			}
		}
		
		return turns;
	}
	
	private static List<Turn> getForQueen(Piece[][] tab, int i, int j) {
		List<Turn> turns = new ArrayList<Turn>();
		
		for(int b = j + 1; b < 8; b++) {
			if(tab[i][b].key != PKey.NONE && tab[i][b].color == tab[i][j].color)
				break;
			turns.add(new Turn(tab[i][j], tab[i][b], i, j, i, b, tab[i][b].key.getValue()));
			if(tab[i][b].key != PKey.NONE)
				break;
		}
		
		for(int b = j - 1; b >= 0; b--) {
			if(tab[i][b].key != PKey.NONE && tab[i][b].color == tab[i][j].color)
				break;
			turns.add(new Turn(tab[i][j], tab[i][b], i, j, i, b, tab[i][b].key.getValue()));
			if(tab[i][b].key != PKey.NONE)
				break;
		}
		
		for(int a = i + 1; a < 8; a++) {
			if(tab[a][j].key != PKey.NONE && tab[a][j].color == tab[i][j].color)
				break;
			turns.add(new Turn(tab[i][j], tab[a][j], i, j, a, j, tab[a][j].key.getValue()));
			if(tab[a][j].key != PKey.NONE)
				break;
		}
		
		for(int a = i - 1; a >= 0; a--) {
			if(tab[a][j].key != PKey.NONE && tab[a][j].color == tab[i][j].color)
				break;
			turns.add(new Turn(tab[i][j], tab[a][j], i, j, a, j, tab[a][j].key.getValue()));
			if(tab[a][j].key != PKey.NONE)
				break;
		}
		
		for(int a = i - 1, b = j - 1; a >= 0 && b >= 0; a--, b--) {
			if(tab[a][b].key != PKey.NONE && tab[a][b].color == tab[i][j].color)
				break;
			turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
			if(tab[a][b].key != PKey.NONE)
				break;
		}
		
		for(int a = i + 1, b = j + 1; a < 8 && b < 8; a++, b++) {
			if(tab[a][b].key != PKey.NONE && tab[a][b].color == tab[i][j].color)
				break;
			turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
			if(tab[a][b].key != PKey.NONE)
				break;
		}
		
		for(int a = i + 1, b = j - 1; a < 8 && b >= 0; a++, b--) {
			if(tab[a][b].key != PKey.NONE && tab[a][b].color == tab[i][j].color)
				break;
			turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
			if(tab[a][b].key != PKey.NONE)
				break;
		}
		
		for(int a = i - 1, b = j + 1; a >= 0 && b < 8; a--, b++) {
			if(tab[a][b].key != PKey.NONE && tab[a][b].color == tab[i][j].color)
				break;
			turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
			if(tab[a][b].key != PKey.NONE)
				break;
		}
		
		return turns;
	}
	
	private static List<Turn> getForBishoe(Piece[][] tab, int i, int j) {
		List<Turn> turns = new ArrayList<Turn>();
		
		for(int a = i - 1, b = j - 1; a >= 0 && b >= 0; a--, b--) {
			if(tab[a][b].key != PKey.NONE && tab[a][b].color == tab[i][j].color)
				break;
			turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
			if(tab[a][b].key != PKey.NONE)
				break;
		}
		
		for(int a = i + 1, b = j + 1; a < 8 && b < 8; a++, b++) {
			if(tab[a][b].key != PKey.NONE && tab[a][b].color == tab[i][j].color)
				break;
			turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
			if(tab[a][b].key != PKey.NONE)
				break;
		}
		
		for(int a = i + 1, b = j - 1; a < 8 && b >= 0; a++, b--) {
			if(tab[a][b].key != PKey.NONE && tab[a][b].color == tab[i][j].color)
				break;
			turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
			if(tab[a][b].key != PKey.NONE)
				break;
		}
		
		for(int a = i - 1, b = j + 1; a >= 0 && b < 8; a--, b++) {
			if(tab[a][b].key != PKey.NONE && tab[a][b].color == tab[i][j].color)
				break;
			turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
			if(tab[a][b].key != PKey.NONE)
				break;
		}
		
		return turns;
	}
	
	private static List<Turn> getForKnight(Piece[][] tab, int i, int j) {
		List<Turn> turns = new ArrayList<Turn>();
		int a, b;
		
		a = i + 1;
		b = j + 2;
		if(isValid(a, b) && tab[a][b].color != tab[i][j].color) {
			turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
		}
		
		a = i + 2;
		b = j + 1;
		if(isValid(a, b) && tab[a][b].color != tab[i][j].color) {
			turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
		}
		
		a = i - 1;
		b = j - 2;
		if(isValid(a, b) && tab[a][b].color != tab[i][j].color) {
			turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
		}
		
		a = i - 2;
		b = j - 1;
		if(isValid(a, b) && tab[a][b].color != tab[i][j].color) {
			turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
		}
		
		a = i + 1;
		b = j - 2;
		if(isValid(a, b) && tab[a][b].color != tab[i][j].color) {
			turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
		}
		
		a = i + 2;
		b = j - 1;
		if(isValid(a, b) && tab[a][b].color != tab[i][j].color) {
			turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
		}
		
		a = i - 1;
		b = j + 2;
		if(isValid(a, b) && tab[a][b].color != tab[i][j].color) {
			turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
		}
		
		a = i - 2;
		b = j + 1;
		if(isValid(a, b) && tab[a][b].color != tab[i][j].color) {
			turns.add(new Turn(tab[i][j], tab[a][b], i, j, a, b, tab[a][b].key.getValue()));
		}
		
		return turns;
	}
	
	private static List<Turn> getForRook(Piece[][] tab, int i, int j) {
		List<Turn> turns = new ArrayList<Turn>();
		
		for(int b = j + 1; b < 8; b++) {
			if(tab[i][b].key != PKey.NONE && tab[i][b].color == tab[i][j].color)
				break;
			turns.add(new Turn(tab[i][j], tab[i][b], i, j, i, b, tab[i][b].key.getValue()));
			if(tab[i][b].key != PKey.NONE)
				break;
		}
		
		for(int b = j - 1; b >= 0; b--) {
			if(tab[i][b].key != PKey.NONE && tab[i][b].color == tab[i][j].color)
				break;
			turns.add(new Turn(tab[i][j], tab[i][b], i, j, i, b, tab[i][b].key.getValue()));
			if(tab[i][b].key != PKey.NONE)
				break;
		}
		
		for(int a = i + 1; a < 8; a++) {
			if(tab[a][j].key != PKey.NONE && tab[a][j].color == tab[i][j].color)
				break;
			turns.add(new Turn(tab[i][j], tab[a][j], i, j, a, j, tab[a][j].key.getValue()));
			if(tab[a][j].key != PKey.NONE)
				break;
		}
		
		for(int a = i - 1; a >= 0; a--) {
			if(tab[a][j].key != PKey.NONE && tab[a][j].color == tab[i][j].color)
				break;
			turns.add(new Turn(tab[i][j], tab[a][j], i, j, a, j, tab[a][j].key.getValue()));
			if(tab[a][j].key != PKey.NONE)
				break;
		}
		
		return turns;
	}
		
	private static boolean isValid(int i, int j) {
		return j >= 0 && i >= 0 && i < 8 && j < 8;
	}
	
	public static void showTab(Piece[][] tab) {
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				Piece p = tab[i][j];
				System.out.print("" + p + " ");
			}
			System.out.println("");
		}
		System.out.println("\n\n");
	}
	
	private static Piece fromIChessPiece(IChessPiece piece) {
		PColor c = PColor.NONE;
		PKey k = PKey.NONE;
		switch(piece.getKey()) {
			case KING:
				k = PKey.KING;
				break;
			case QUEEN:
				k = PKey.QUEEN;
				break;
			case BISHOP:
				k = PKey.BISHOP;
				break;
			case KNIGHT:
				k = PKey.KNIGHT;
				break;
			case ROOK:
				k = PKey.ROOK;
				break;
			case PAWN:
				k = PKey.PAWN;
				break;
		}

		switch(piece.getColor()) {
			case BLACK:
				c = PColor.BLACK;
				break;
			case WHITE:
				c = PColor.WHITE;
		}

		return new Piece(k, c);
	}

	public static Piece[][] fromPieces(List<ChessPiece> pieces) {
		Piece[][] tab = new Piece[8][8];

		for(int i = 0; i < 8; i++)
			for(int j = 0; j < 8; j++)
				tab[i][j] = Piece.getNone();

		for(ChessPiece p: pieces) {
			if(!p.isCaptured()) {
				Piece np = fromIChessPiece(p);
				tab[p.getI()][p.getJ()] = np;
			}
		}

		return tab;
	}



	public static boolean isCheck(Piece[][] tab, PColor color) {
		PColor otherColor = color.getInverse();

		List<Turn> turns = getTurnsFor(tab, otherColor);

		for(Turn t: turns) {
			if(t.np.key == PKey.KING) {
				return true;
			}
		}

		return false;
	}

	public static boolean isCheckmate(Piece[][] tab, PColor color) {
		if(isCheck(tab, color)) {
			List<Turn> turns = getTurnsFor(tab, color);

			if(turns != null) {
				for(Turn t: turns) {
					t.makeTurn(tab);

					if(!isCheck(tab, color))
						return false;

					t.goBack(tab);
				}

				return true;
			}

			return false;
        }
		return false;
	}

	public static List<Turn> getTurnsToRoque(Piece[][] tab, PColor color) {
		List<Turn> turns = new ArrayList<Turn>();
		if(color == PColor.WHITE) {
			Piece king = tab[0][3];
			Piece r1 = tab[0][0];
			Piece r2 = tab[0][7];

			if(king.key == PKey.KING) {
				if(r1.key == PKey.ROOK && tab[0][1].key == PKey.NONE && tab[0][2].key == PKey.NONE) {
					Turn t = new Turn(king, Piece.getNone(), 0, 3, 0, 1, 0);
					t.makeTurn(tab);
					if(!isCheck(tab, color)) {
						turns.add(t);
						turns.add(new Turn(r1, Piece.getNone(), 0, 0, 0, 2, 0));
					}
					t.goBack(tab);

				} if(r2.key == PKey.ROOK && tab[0][4].key == PKey.NONE && tab[0][5].key == PKey.NONE && tab[0][6].key == PKey.NONE) {
					Turn t = new Turn(king, Piece.getNone(), 0, 3, 0, 5, 0);
					t.makeTurn(tab);
					if(!isCheck(tab, color)) {
						turns.add(t);
						turns.add(new Turn(r2, Piece.getNone(), 0, 7, 0, 4, 0));
					}
					t.goBack(tab);

				}
			}

		} else {
			Piece king = tab[7][3];
			Piece r1 = tab[7][0];
			Piece r2 = tab[7][7];

			if(king.key == PKey.KING) {
				if(r1.key == PKey.ROOK && tab[7][1].key == PKey.NONE && tab[7][2].key == PKey.NONE) {
					Turn t = new Turn(king, Piece.getNone(), 7, 3, 7, 1, 0);
					t.makeTurn(tab);
					if(!isCheck(tab, color)) {
						turns.add(t);
						turns.add(new Turn(r1, Piece.getNone(), 7, 0, 7, 2, 0));
					}
					t.goBack(tab);

				} if(r2.key == PKey.ROOK && tab[7][4].key == PKey.NONE && tab[7][5].key == PKey.NONE && tab[7][6].key == PKey.NONE) {
					Turn t = new Turn(king, Piece.getNone(), 7, 3, 7, 5, 0);

					t.makeTurn(tab);
					if(!isCheck(tab, color)) {
						turns.add(t);
						turns.add(new Turn(r2, Piece.getNone(), 7, 7, 7, 4, 0));
					}
					t.goBack(tab);

				}
			}
		}

		return turns;
	}
}
