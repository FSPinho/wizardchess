package felipe.wizardchess.application.algorithm;

import felipe.wizardchess.application.chess.ChessComand;

public class Turn {
	public Piece tp = Piece.getNone(), np = Piece.getNone();
	public int ti = -1, tj = -1;
	public int ni = -1, nj = -1;
	public int value = 0;
	
	public int min = 0;
	
	public Turn(Piece tp, Piece np, int ti, int tj, int ni, int nj, int value) {
		this.tp = tp;
		this.np = np;
		this.ti = ti;
		this.tj = tj;
		this.ni = ni;
		this.nj = nj;
		this.value = value;
	}

	public void makeTurn(Piece[][] tab) {
		tab[ti][tj] = Piece.getNone();
		tab[ni][nj] = tp;
	}
	
	public void goBack(Piece[][] tab) {
		tab[ti][tj] = tp;
		tab[ni][nj] = np;
	}

	@Override
	public String toString() {
		return "Turn [tp=" + tp + ", np=" + np + ", ti=" + ti + ", tj=" + tj
				+ ", ni=" + ni + ", nj=" + nj + ", value=" + value + "]";
	}

}
