package felipe.wizardchess.application.algorithm;

public class Piece {
	public PKey key;
	public PColor color;
	
	public Piece(PKey key, PColor color) {
		this.key = key;
		this.color = color;
	}
	
	public static Piece getNone() {
		return new Piece(PKey.NONE, PColor.NONE);
	}
	
	@Override
	public String toString() {
		return "" + key + color;
	}
}
