package felipe.wizardchess.application.algorithm;

public enum PKey {
	KING, 
	QUEEN,
	BISHOP,
	KNIGHT, 
	ROOK, 
	PAWN, 
	NONE;
	
	public int getValue() {
		switch (this) {
		case KING:
			return 100;
		case QUEEN:
			return 9;
		case BISHOP:
			return 3;
		case KNIGHT:
			return 3;
		case ROOK:
			return 5;
		case PAWN:
			return 1;
		default:
			return 0;
		}
	}
	
	@Override
	public String toString() {
		switch (this) {
		case KING:
			return "K";
		case QUEEN:
			return "Q";
		case BISHOP:
			return "B";
		case KNIGHT:
			return "H";
		case ROOK:
			return "R";
		case PAWN:
			return "P";
		default:
			return "_";
		}
	}
}
