package felipe.wizardchess.application.transition;

import java.util.List;
import java.util.Vector;

public abstract class BTransition implements IBTransition {
	private int interval;
	private int delay;
	private List<IBTransitionTask> tasks;
	private IBTransitionTask task;
	private boolean waitForCompletion;
	private boolean valid = false;
	
	public BTransition(int interval, int delay) {
		this(interval, delay, false);
	}
	
	public BTransition(int interval, int delay, boolean waitForCompletion) {
		this.interval = interval;
		this.delay = delay;
		this.tasks = new Vector<IBTransitionTask>();
		this.waitForCompletion = waitForCompletion;
	}

	@Override
	public void addTransition(double origin, double dest) {
		if(waitForCompletion) {
			if(tasks.size() > 0) 
				tasks.add(new BTransitionTask(tasks.get(tasks.size() - 1).getDest(), dest, interval, delay));
			else
				tasks.add(new BTransitionTask(origin, dest, interval, delay));
		} else {
			tasks.clear();
			tasks.add(new BTransitionTask(origin, dest, interval, delay));
		}
		
		valid = true;
	}
	
	public int getInterval() {
		return interval;
	}
	
	public void setInterval(int interval) {
		this.interval = interval;
	}
	
	@Override
	public boolean isValid() {
		return valid;
	}
	
	@Override
	public boolean isCompleted() {
		return tasks.size() == 0;
	}
	
	@Override
	public boolean isWaitForCompletion() {
		return waitForCompletion;
	}
	
	@Override
	public void setWaitForCompletion(boolean wait) {
		this.waitForCompletion = wait;
	}
	
	protected IBTransitionTask getNextTask() {
		if(tasks.size() > 0) {
			task = tasks.get(0);
		}
		
		if(task != null && !task.isValid())
			tasks.remove(task);
		
		return task;
	}
}
