package felipe.wizardchess.application.transition;

public class BTransitionExpoOut extends BTransition {

	public BTransitionExpoOut(int interval, int delay) {
		super(interval, delay);
	}
	
	public BTransitionExpoOut(int interval, int delay, boolean waitForCompletion) {
		super(interval, delay, waitForCompletion);
	}

	@Override
	public double getNextValue() {
		IBTransitionTask task = getNextTask();
		
		if(task != null && task.isValid()) {
			double s0 = task.getOrigin();
			double s = task.getDest();
			double v0 = 0;
			double t = getInterval();
			double a = (s - s0 - v0*t)*2/(t*t);
			
			t = task.getStep();
			double currentValue = s0 + v0*t + a*t*t/2;
			task.nextStep();
			task.setCurrentValue(currentValue);

		} else {
			task.setCurrentValue(task.getDest());
		}

		return task.getCurrentValue();
	}

}
