package felipe.wizardchess.application.transition;

public class BTransitionLinear extends BTransition {

	public BTransitionLinear(int interval, int delay) {
		super(interval, delay);
	}
	
	public BTransitionLinear(int interval, int delay, boolean waitForCompletion) {
		super(interval, delay, waitForCompletion);
	}

	@Override
	public double getNextValue() {
		IBTransitionTask task = getNextTask();

		if(task != null && task.isValid()) {
			double distance = task.getDest() - task.getOrigin();
			double d = distance * task.getStep() / (double) getInterval();
			task.nextStep();
			task.setCurrentValue(task.getOrigin() + d);

		} else {
			task.setCurrentValue(task.getDest());
		}

		return task.getCurrentValue();
	}
	
}
