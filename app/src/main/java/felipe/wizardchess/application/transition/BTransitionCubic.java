package felipe.wizardchess.application.transition;

import android.opengl.Matrix;
import android.util.Log;

/**
 * Created by felipe on 17/07/15.
 */
public class BTransitionCubic extends BTransition {
    private static final String LOGTAG = "Transition cubic";

    private final float[] M = {
            -1, 3, -3, 1,
            2, -5, 4, -1,
            -1, 0, 1, 0,
            0, 2, 0, 0
    };

    private final float[] M2 = {
            -1, 3, -3, 1,
            3, -6, 3, 0,
            -3, 0, 3, 0,
            1, 4, 1, 0
    };

    private float[] p0;
    private float[] p1;
    private float[] p2;
    private float[] p3;
    private float[] p4;
    private float[] p5;
    private float[] p6;
    private float[] pointsAll;

    float heightPiece = 0.0f;
    float heightJump = 0.0f;
    float distance = 0.0f;

    int curves = 0;
    int intervalByCurve = 0;

    public BTransitionCubic(int interval, int delay, float heightPiece, float heightJump) {
        super(interval, delay);

        for(int i = 0; i < M.length; i++) {
            M[i] *= 1.0/2.0;
        }

        this.heightPiece = heightPiece;
        this.heightJump = heightJump;

        curves = 7 - 3;
        intervalByCurve = interval/curves;
    }

    @Override
    public void addTransition(double origin, double dest) {
        this.distance = heightJump*6;
        float x0 = 0.0f;

        float y0 = (float) origin;
        float y1 = (float) dest;

        p0 = new float[] {x0 + heightJump*0, y0 - 1.0f, 0.0f};
        p1 = new float[] {x0 + heightJump*1, y0, 0.0f};
        p2 = new float[] {x0 + heightJump*2, y0 + heightPiece, 0.0f};
        p3 = new float[] {x0 + heightJump*3, y0 + heightJump, 0.0f};
        p4 = new float[] {x0 + heightJump*4, y1 + heightPiece, 0.0f};
        p5 = new float[] {x0 + heightJump*5, y1, 0.0f};
        p6 = new float[] {x0 + heightJump*6, y1 - 1.0f, 0.0f};
        pointsAll = new float[] {
                p0[0], p0[1], p0[2],
                p1[0], p1[1], p1[2],
                p2[0], p2[1], p2[2],
                p3[0], p3[1], p3[2],
                p4[0], p4[1], p4[2],
                p5[0], p5[1], p5[2],
                p6[0], p6[1], p6[2],
        };

        //Log.i(LOGTAG, "Moving cubit p0(" + p0[0] + " : " + p0[1] + " : " + p0[2] + ")");
        //Log.i(LOGTAG, "Moving cubit p1(" + p1[0] + " : " + p1[1] + " : " + p1[2] + ")");
        //Log.i(LOGTAG, "Moving cubit p2(" + p2[0] + " : " + p2[1] + " : " + p2[2] + ")");
        //Log.i(LOGTAG, "Moving cubit p3(" + p3[0] + " : " + p3[1] + " : " + p3[2] + ")");
        //Log.i(LOGTAG, "Moving cubit p4(" + p4[0] + " : " + p4[1] + " : " + p4[2] + ")");
        //Log.i(LOGTAG, "Moving cubit p5(" + p5[0] + " : " + p5[1] + " : " + p5[2] + ")");
        //Log.i(LOGTAG, "Moving cubit p6(" + p6[0] + " : " + p6[1] + " : " + p6[2] + ")");

        super.addTransition(origin, dest);
    }

    @Override
    public double getNextValue() {
        IBTransitionTask task = getNextTask();
        if(task.isValid()) {
            int firstPoint = task.getStep() / intervalByCurve;

            float u = task.getStep() % intervalByCurve;
            u = (float) u / intervalByCurve;
            float[] ut = {
                    u*u*u, u*u, u, 1,
                    0, 0, 0, 0,
                    0, 0, 0, 0,
                    0, 0, 0, 0,
            };

            int i = firstPoint*3;
            float[] points = new float[] {
                    pointsAll[i], pointsAll[i + 1], pointsAll[i + 2], 0,
                    pointsAll[i + 3], pointsAll[i + 4], pointsAll[i + 5], 0,
                    pointsAll[i + 6], pointsAll[i + 7], pointsAll[i + 8], 0,
                    pointsAll[i + 9], pointsAll[i + 10], pointsAll[i + 11], 0
            };

            float[] res1 = new float[16];
            float[] res2 = new float[16];
            Matrix.multiplyMM(res1, 0, M, 0, ut, 0);
            Matrix.multiplyMM(res2, 0, points, 0, res1, 0);

            task.nextStep();
            double y = res2[1];
            task.setCurrentValue(y);

            //Log.i(LOGTAG, "Moving(" + res2[0] + " : " + res2[1] + " : " + res2[2] + ")");
            //Log.i(LOGTAG, "Y's are(" + points[1] + " : " + points[5] + " : " + points[9] + " : " + points[13] + ")");
            //Log.i(LOGTAG, "First: " + firstPoint);

        } else {
            task.setCurrentValue(task.getDest());
        }
        return task.getCurrentValue();
    }
}
