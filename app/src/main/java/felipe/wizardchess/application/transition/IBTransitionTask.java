package felipe.wizardchess.application.transition;

public interface IBTransitionTask {
	public double getOrigin();
	public double getDest();
	
	public void setStep(int step);
	public int getStep();
	public int getStepCount();
	public boolean nextStep();
	
	public void setCurrentValue(double value);
	public double getCurrentValue();
	
	public boolean isValid();
}
