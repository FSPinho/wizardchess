package felipe.wizardchess.application.transition;

public interface IBTransition {
	public void addTransition(double origin, double dest);
	
	public boolean isValid();
	public boolean isCompleted();
	public boolean isWaitForCompletion();
	public void setWaitForCompletion(boolean wait);
	
	public double getNextValue();
}
