package felipe.wizardchess.application.transition;

public class BTransitionTask implements IBTransitionTask {
	private long TASK_COUNT = 0;
	
	private long id;
	private double origin;
	private double dest;
	private double currentValue;
	private int stepCount;
	private int step;
	private int delay;
	
	public BTransitionTask(double origin, double dest, int stepCount, int delay) {
		this.id = TASK_COUNT++;
		this.origin = currentValue = origin;
		this.dest = dest;
		this.stepCount = stepCount;
		this.step = 0;
		this.delay = delay;
	}
	
	public long getId() {
		return id;
	}
	
	@Override
	public double getOrigin() {
		return origin;
	}

	@Override
	public double getDest() {
		return dest;
	}

	@Override
	public void setStep(int step) {
		this.step = step;
	}

	@Override
	public int getStep() {
		return step;
	}

	@Override
	public int getStepCount() {
		return stepCount;
	}

	@Override
	public boolean nextStep() {
		if(delay > 0) {
			delay--;
			return true;
		} else if(step < stepCount) {
			step++;
			return true;
		}
		
		return false;
	}

	@Override
	public void setCurrentValue(double value) {
		this.currentValue = value;
	}

	@Override
	public double getCurrentValue() {
		return currentValue;
	}

	@Override
	public boolean equals(Object obj) {
		BTransitionTask t = (BTransitionTask) obj;
		return id == t.getId();
	}
	
	@Override
	public boolean isValid() {
		return step < stepCount;
	}
}
