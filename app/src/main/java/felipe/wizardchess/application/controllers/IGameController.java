package felipe.wizardchess.application.controllers;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.qualcomm.vuforia.CameraDevice;
import com.qualcomm.vuforia.DataSet;
import com.qualcomm.vuforia.ObjectTracker;
import com.qualcomm.vuforia.STORAGE_TYPE;
import com.qualcomm.vuforia.State;
import com.qualcomm.vuforia.Trackable;
import com.qualcomm.vuforia.Tracker;
import com.qualcomm.vuforia.TrackerManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import codeon.bdialog.IChoiceItem;
import codeon.bdialog.IDialogListener;
import codeon.bdialog.StandardDialog;
import codeon.bdialogconcret.ChoiceItem;
import codeon.bdialogconcret.DialogMultipleChoice;
import felipe.wizardchess.R;
import felipe.wizardchess.application.algorithm.PColor;
import felipe.wizardchess.application.algorithm.Piece;
import felipe.wizardchess.application.algorithm.Turn;
import felipe.wizardchess.application.algorithm.Util;
import felipe.wizardchess.application.chess.ChessPiece;
import felipe.wizardchess.application.chess.ChessSet;
import felipe.wizardchess.application.chess.IChessComand;
import felipe.wizardchess.application.chess.IChessPiece;
import felipe.wizardchess.application.chess.IChessPieceListener;
import felipe.wizardchess.application.speechrecognition.ISpeechListener;
import felipe.wizardchess.application.speechrecognition.SpeechRecognizer;
import felipe.wizardchess.ui.GameActivity;

/**
 * Created by felipe on 22/07/15.
 */
public class IGameController implements AppControll, IChessPieceListener, ISpeechListener, IDialogListener {
    public static String LOGTAG = "APP_CONTROLL";

    private static final int ADVANCE_BY_SEC = 100;

    private DataSet currentDataset;
    private int currentDatasetSelectionIndex = 0;
    private int startDatasetsIndex = 0;
    private int datasetsNumber = 0;
    private ArrayList<String> datasetStrings = new ArrayList<String>();
    private boolean switchDatasetAsap = false;
    private boolean mContAutofocus = false;
    private boolean extendedTracking = false;
    private AlertDialog errorDialog;

    private Runnable action;

    private AppSession appSession;

    private GameActivity activity;

    ChessSet chessSet;

    SpeechRecognizer speechRecognizer;

    enum GamePlayer {USER, COMPUTER};
    GamePlayer currentPlayer = GamePlayer.USER;

    private IChessComand currentComand;
    private ChessPiece choice = null;
    private Map<Long, ChessPiece> choices = null;

    public IGameController(GameActivity activity) {
        this.activity = activity;
    }

    public void doInit() {
        appSession = new AppSession(this);

        choices = new HashMap<>();

        //datasetStrings.add("StonesAndChips.xml");
        //datasetStrings.add("Tarmac.xml");
        datasetStrings.add("WizardChessDB/WizardChessDB.xml");

        appSession.initAR(activity, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        chessSet = new ChessSet(activity);
        chessSet.setSwitchListener(this);

        action = new Runnable() {
            @Override
            public void run() {
                advance();
            }
        };

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                activity.runOnUiThread(action);
            }
        }, 1000, 1000 / ADVANCE_BY_SEC);
    }

    public void advance() {
        chessSet.advance();
    }

    @Override
    public boolean doInitTrackers() {
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        Tracker tracker;

        // Trying to initialize the image tracker
        tracker = tManager.initTracker(ObjectTracker.getClassType());
        if (tracker == null) {
            Log.e(LOGTAG, "Tracker not initialized. Tracker already initialized or the camera is already started");
            result = false;
        } else {
            Log.i(LOGTAG, "Tracker successfully initialized");
        }
        return result;
    }

    // Methods to load and destroy tracking data.
    @Override
    public boolean doLoadTrackersData() {
        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager
                .getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        if (currentDataset == null)
            currentDataset = objectTracker.createDataSet();

        if (currentDataset == null)
            return false;

        if (!currentDataset.load(
                datasetStrings.get(currentDatasetSelectionIndex),
                STORAGE_TYPE.STORAGE_APPRESOURCE))
            return false;

        if (!objectTracker.activateDataSet(currentDataset))
            return false;

        int numTrackables = currentDataset.getNumTrackables();
        for (int count = 0; count < numTrackables; count++) {
            Trackable trackable = currentDataset.getTrackable(count);
            if(extendedTracking) {
                trackable.startExtendedTracking();
            }

            String name = "Current Dataset : " + trackable.getName();
            trackable.setUserData(name);
            Log.d(LOGTAG, "UserData:Set the following user data "
                    + (String) trackable.getUserData());
        }

        return true;
    }

    @Override
    public boolean doStartTrackers() {
        // Indicate if the trackers were started correctly
        boolean result = true;

        Tracker objectTracker = TrackerManager.getInstance().getTracker(ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.start();

        return result;
    }

    @Override
    public boolean doStopTrackers() {
        // Indicate if the trackers were stopped correctly
        boolean result = true;

        Tracker objectTracker = TrackerManager.getInstance().getTracker(
                ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.stop();

        return result;
    }

    @Override
    public boolean doUnloadTrackersData() {
        // Indicate if the trackers were unloaded correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager.getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        if (currentDataset != null && currentDataset.isActive()) {
            if (objectTracker.getActiveDataSet().equals(currentDataset)
                    && !objectTracker.deactivateDataSet(currentDataset)) {
                result = false;
            } else if (!objectTracker.destroyDataSet(currentDataset)) {
                result = false;
            }

            currentDataset = null;
        }

        return result;
    }

    @Override
    public boolean doDeinitTrackers() {
        // Indicate if the trackers were deinitialized correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        tManager.deinitTracker(ObjectTracker.getClassType());

        return result;
    }

    @Override
    public void onInitARDone(AppException exception) {

        if (exception == null) {
            initApplicationAR();

            try {
                appSession.startAR(CameraDevice.CAMERA.CAMERA_DEFAULT);
            } catch (AppException e) {
                Log.e(LOGTAG, e.getString());
            }

            boolean result = CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO);

            if (result)
                mContAutofocus = true;
            else
                Log.e(LOGTAG, "Unable to enable continuous autofocus");

            activity.loadUiLayout();

        } else {
            Log.e(LOGTAG, exception.getString());
            showInitializationErrorMessage(exception.getString());
        }
    }

    // Shows initialization error messages as System dialogs
    public void showInitializationErrorMessage(String message) {
        final String errorMessage = message;
        activity.runOnUiThread(new Runnable() {
            public void run() {
                if (errorDialog != null) {
                    errorDialog.dismiss();
                }

                // Generates an Alert Dialog to show the error message
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder
                        .setMessage(errorMessage)
                        .setTitle(activity.getString(R.string.INIT_ERROR))
                        .setCancelable(false)
                        .setIcon(0)
                        .setPositiveButton(activity.getString(R.string.button_OK),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        activity.finish();
                                    }
                                });

                errorDialog = builder.create();
                errorDialog.show();
            }
        });
    }

    @Override
    public void onQCARUpdate(State state) {
        if (switchDatasetAsap) {
            switchDatasetAsap = false;
            TrackerManager tm = TrackerManager.getInstance();
            ObjectTracker ot = (ObjectTracker) tm.getTracker(ObjectTracker .getClassType());
            if (ot == null || currentDataset == null || ot.getActiveDataSet() == null) {
                Log.d(LOGTAG, "Failed to swap datasets");
                return;
            }

            doUnloadTrackersData();
            doLoadTrackersData();
        }
    }

    // Initializes AR application components.
    private void initApplicationAR() {
        activity.initGL(appSession, chessSet);
    }

    public void resumeAR() throws AppException {
        appSession.resumeAR();
    }

    public void pauseAR() throws AppException {
        appSession.pauseAR();
    }

    public void stopAR() throws AppException {
        appSession.stopAR();
    }

    public void onConfigurationChanged() {
        appSession.onConfigurationChanged();
    }

    @Override
    public ChessPiece onSwitchPiece(List<ChessPiece> pieces) {
        if(choice == null) {
            DialogMultipleChoice d = new DialogMultipleChoice();
            d.setTitle("Que peça deseja mover?");
            List<IChoiceItem> items = new ArrayList<>();

            for(ChessPiece p: pieces) {
                ChoiceItem item = new ChoiceItem(p.getId(), p.toString());
                choices.put((long)p.getId(), p);
                items.add(item);
            }
            d.setItems(items);
            d.addDialogListener(this);
            d.show(0, activity.getSupportFragmentManager().beginTransaction());
        }

        ChessPiece aux = choice;
        choice = null;
        return aux;
    }

    @Override
    public void onPieceMoved(IChessPiece p) {
        nextMovie();
    }

    public void getMoveFromSpeech() {
        speechRecognizer = activity.getSpeech();
        speechRecognizer.addListener(this);
        speechRecognizer.startListening();
    }

    @Override
    public void onSpeechResult(IChessComand comand) {
        currentComand = comand;
        if(comand != null && chessSet.execComand(comand) != null) {
            activity.showToast("Exec command!");
        } else {
            activity.showToast("Invalid move!");
        }
    }

    private void nextMovie() {
        if(isMate()) {

        } else if(currentPlayer == GamePlayer.COMPUTER) {
            activity.freeControlls();
            currentPlayer = GamePlayer.USER;
        } else {
            activity.blockControlls();
            currentPlayer = GamePlayer.COMPUTER;
            AsyncMove move = new AsyncMove();
            move.execute(chessSet);
        }
    }

    private boolean isMate() {
        return false;
    }

    class AsyncMove extends AsyncTask<ChessSet, Integer, Turn> {
        @Override
        protected Turn doInBackground(ChessSet... params) {
            ChessSet chessSet = params[0];
            //showToast("Other moving...");
            Log.i(LOGTAG, "Calculating...");
            Piece[][] tab = Util.fromPieces(chessSet.getPieces());
            Turn t = Util.getNextTurn(tab, PColor.BLACK, 4);
            Log.i(LOGTAG, "Loops: " + Util.loop + " - " + t.toString());
            return t;
        }

        @Override
        protected void onPostExecute(Turn turn) {
            Util.loop = 0;
            chessSet.execTurn(turn);
        }
    }

    @Override
    public void onFinished(int result, int code, Object... objects) {
        if(result == StandardDialog.RESULT_OK) {
            long id = (long)(objects[0]);
            choice = choices.get(id);
            chessSet.execComand(currentComand);
        }
    }
}
