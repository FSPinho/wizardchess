package felipe.wizardchess.application.controllers;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

import com.qualcomm.vuforia.CameraCalibration;
import com.qualcomm.vuforia.CameraDevice;
import com.qualcomm.vuforia.Matrix44F;
import com.qualcomm.vuforia.Renderer;
import com.qualcomm.vuforia.State;
import com.qualcomm.vuforia.Tool;
import com.qualcomm.vuforia.Vec2I;
import com.qualcomm.vuforia.VideoBackgroundConfig;
import com.qualcomm.vuforia.VideoMode;
import com.qualcomm.vuforia.Vuforia;

import felipe.wizardchess.R;

/**
 * Created by felipe on 08/07/15.
 */
public class AppSession implements Vuforia.UpdateCallbackInterface {
    public static final String LOGTAG = "WCAppSession";

    // Reference to the current activity
    private Activity activity;
    private AppControll controller;

    // Flags
    private boolean started = false;
    private boolean cameraRunning = false;

    // Display size of the device:
    private int screenWidth = 0;
    private int screenHeight = 0;

    // The async tasks to initialize the Vuforia SDK:
    private InitVuforiaTask mInitVuforiaTask;
    private LoadTrackerTask mLoadTrackerTask;

    // An object used for synchronizing Vuforia initialization, dataset loading
    // and the Android onDestroy() life cycle event. If the application is
    // destroyed while a data set is still being loaded, then we wait for the
    // loading operation to finish before shutting down Vuforia:
    private Object shutdownLock = new Object();

    // Vuforia initialization flags:
    private int vuforiaFlags = 0;

    // Holds the camera configuration to use upon resuming
    private int camera = CameraDevice.CAMERA.CAMERA_DEFAULT;

    // Stores the projection matrix to use for rendering purposes
    private Matrix44F projectionMatrix;

    // Stores orientation
    private boolean isPortrait = false;

    public AppSession(AppControll controller) {
        this.controller = controller;
    }

    public void initAR(Activity activity, int screenOrientation) {
        AppException exception = null;
        this.activity = activity;

        if ((screenOrientation == ActivityInfo.SCREEN_ORIENTATION_SENSOR) && (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO))
            screenOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR;

        // Apply screen orientation
        this.activity.setRequestedOrientation(screenOrientation);
        updateActivityOrientation();

        storeScreenDimensions();

        this.activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        vuforiaFlags = Vuforia.GL_20;

        if (mInitVuforiaTask != null) {
            String logMessage = "Cannot initialize SDK twice";
            exception = new AppException(AppException.VUFORIA_ALREADY_INITIALIZATED, logMessage);
            Log.e(LOGTAG, logMessage);
        }

        if (exception == null) {
            try {
                mInitVuforiaTask = new InitVuforiaTask();
                mInitVuforiaTask.execute();
            } catch (Exception e) {
                String logMessage = "Initializing Vuforia SDK failed";
                exception = new AppException(AppException.INITIALIZATION_FAILURE, logMessage);
                Log.e(LOGTAG, logMessage);
            }
        }

        if (exception != null)
            controller.onInitARDone(exception);
    }

    // Starts Vuforia, initialize and starts the camera and start the trackers
    public void startAR(int camera) throws AppException {
        String error = "";
        if(cameraRunning) {
            error = "Camera already running, unable to open again";
            Log.e(LOGTAG, error);
            throw new AppException(AppException.CAMERA_INITIALIZATION_FAILURE, error);
        }

        this.camera = camera;
        if (!CameraDevice.getInstance().init(this.camera)) {
            error = "Unable to open camera device: " + camera;
            Log.e(LOGTAG, error);
            throw new AppException(AppException.CAMERA_INITIALIZATION_FAILURE, error);
        }

        configureVideoBackground();

        if (!CameraDevice.getInstance().selectVideoMode(CameraDevice.MODE.MODE_DEFAULT)) {
            error = "Unable to set video mode";
            Log.e(LOGTAG, error);
            throw new AppException(AppException.CAMERA_INITIALIZATION_FAILURE, error);
        }

        if (!CameraDevice.getInstance().start()) {
            error = "Unable to start camera device: " + camera;
            Log.e(LOGTAG, error);
            throw new AppException(
                    AppException.CAMERA_INITIALIZATION_FAILURE, error);
        }

        setProjectionMatrix();

        controller.doStartTrackers();

        cameraRunning = true;

        if(!CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_TRIGGERAUTO)) {
            CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_NORMAL);
        }
    }

    // Stops any ongoing initialization, stops Vuforia
    public void stopAR() throws AppException {
        // Cancel potentially running tasks
        if (mInitVuforiaTask != null && mInitVuforiaTask.getStatus() != InitVuforiaTask.Status.FINISHED) {
            mInitVuforiaTask.cancel(true);
            mInitVuforiaTask = null;
        }

        if (mLoadTrackerTask != null && mLoadTrackerTask.getStatus() != LoadTrackerTask.Status.FINISHED) {
            mLoadTrackerTask.cancel(true);
            mLoadTrackerTask = null;
        }

        mInitVuforiaTask = null;
        mLoadTrackerTask = null;

        started = false;

        stopCamera();

        // Ensure that all asynchronous operations to initialize Vuforia
        // and loading the tracker datasets do not overlap:
        synchronized (shutdownLock) {
            boolean unloadTrackersResult;
            boolean deinitTrackersResult;

            // Destroy the tracking data set:
            unloadTrackersResult = controller.doUnloadTrackersData();

            // Deinitialize the trackers:
            deinitTrackersResult = controller.doDeinitTrackers();

            // Deinitialize Vuforia SDK:
            Vuforia.deinit();

            if (!unloadTrackersResult)
                throw new AppException(AppException.UNLOADING_TRACKERS_FAILURE, "Failed to unload trackers\' data");

            if (!deinitTrackersResult)
                throw new AppException(AppException.TRACKERS_DEINITIALIZATION_FAILURE, "Failed to deinitialize trackers");

        }
    }

    // Resumes Vuforia, restarts the trackers and the camera
    public void resumeAR() throws AppException {
        // Vuforia-specific resume operation
        Vuforia.onResume();

        if (started) {
            startAR(camera);
        }
    }

    // Pauses Vuforia and stops the camera
    public void pauseAR() throws AppException {
        if (started) {
            stopCamera();
        }

        Vuforia.onPause();
    }

    // Gets the projection matrix to be used for rendering
    public Matrix44F getProjectionMatrix() {
        return projectionMatrix;
    }

    @Override
    public void QCAR_onUpdate(State state) {
        controller.onQCARUpdate(state);
    }

    // Manages the configuration changes
    public void onConfigurationChanged() {
        updateActivityOrientation();

        storeScreenDimensions();

        if (isARRunning()) {
            // configure video background
            configureVideoBackground();

            // Update projection matrix:
            setProjectionMatrix();
        }

    }

    // Methods to be called to handle lifecycle
    public void onResume() {
        Vuforia.onResume();
    }


    public void onPause() {
        Vuforia.onPause();
    }


    public void onSurfaceChanged(int width, int height) {
        Vuforia.onSurfaceChanged(width, height);
    }


    public void onSurfaceCreated() {
        Vuforia.onSurfaceCreated();
    }

    // An async task to initialize Vuforia asynchronously.
    private class InitVuforiaTask extends AsyncTask<Void, Integer, Boolean> {

        // Initialize with invalid value:
        private int progressValue = -1;

        protected Boolean doInBackground(Void... params) {
            // Prevent the onDestroy() method to overlap with initialization:
            synchronized (shutdownLock) {
                Vuforia.setInitParameters(activity, vuforiaFlags, "AYDAW57/////AAAAAW3dkDcu10LsuLZtaItufNEpP8/B+RUmW4Mviv6WYMr9BpMdVe4892dtfQEUWfsfrL8zbKoiZ2uip+9tUZlH+TYN5uXceZT1Ei9L+5bKqGiD75rqX4eExnWCQPTF5AqeIF9brXUQP/49dVfF28okSKhVnU+HJ3ed6M4EfLrLYunqozlWilCUbdtyJ6EDQ9EIUJBP1YpE0t93we9/moKuwLkAde4d6EXjSa1ZX1w73yXNMCHQ99jD5c/2cnLMJrnLDclmSB1trhlixhD+BYB6DiJ9KUl5ZXO5Ue3jm4Z/1gLisq9MHa6pKPLSd/SSqVdl4xmpv4OcaoFH7SNQFTgNL7qHV789llfoxPamS6uZCJnB");

                do {
                    // Vuforia.init() blocks until an initialization step is
                    // complete, then it proceeds to the next step and reports
                    // progress in percents (0 ... 100%).
                    // If Vuforia.init() returns -1, it indicates an error.
                    // Initialization is done when progress has reached 100%.
                    progressValue = Vuforia.init();

                    // Publish the progress value:
                    publishProgress(progressValue);

                    // We check whether the task has been canceled in the
                    // meantime (by calling AsyncTask.cancel(true)).
                    // and bail out if it has, thus stopping this thread.
                    // This is necessary as the AsyncTask will run to completion
                    // regardless of the status of the component that
                    // started is.
                } while (!isCancelled() && progressValue >= 0 && progressValue < 100);

                return (progressValue > 0);
            }
        }


        protected void onProgressUpdate(Integer... values) {
            // Do something with the progress value "values[0]", e.g. update
            // splash screen, progress bar, etc.
        }


        protected void onPostExecute(Boolean result) {
            // Done initializing Vuforia, proceed to next application
            // initialization status:

            AppException exception = null;

            if (result) {
                Log.d(LOGTAG, "InitVuforiaTask.onPostExecute: Vuforia initialization successful");

                boolean initTrackersResult;
                initTrackersResult = controller.doInitTrackers();

                if (initTrackersResult) {
                    try {
                        mLoadTrackerTask = new LoadTrackerTask();
                        mLoadTrackerTask.execute();
                    } catch (Exception e) {
                        String logMessage = "Loading tracking data set failed";
                        exception = new AppException(AppException.LOADING_TRACKERS_FAILURE, logMessage);
                        Log.e(LOGTAG, logMessage);
                        controller.onInitARDone(exception);
                    }

                } else {
                    exception = new AppException(AppException.TRACKERS_INITIALIZATION_FAILURE, "Failed to initialize trackers");
                    controller.onInitARDone(exception);
                }
            } else {
                String logMessage;

                // NOTE: Check if initialization failed because the device is
                // not supported. At this point the user should be informed
                // with a message.
                logMessage = getInitializationErrorString(progressValue);

                // Log error:
                Log.e(LOGTAG, "InitVuforiaTask.onPostExecute: " + logMessage + " Exiting.");

                // Send Vuforia Exception to the application and call initDone
                // to stop initialization process
                exception = new AppException(AppException.INITIALIZATION_FAILURE, logMessage);
                controller.onInitARDone(exception);
            }
        }
    }

    // An async task to load the tracker data asynchronously.
    private class LoadTrackerTask extends AsyncTask<Void, Integer, Boolean> {

        protected Boolean doInBackground(Void... params) {
            // Prevent the onDestroy() method to overlap:
            synchronized (shutdownLock) {
                // Load the tracker data set:
                return controller.doLoadTrackersData();
            }
        }

        protected void onPostExecute(Boolean result) {

            AppException vuforiaException = null;

            Log.d(LOGTAG, "LoadTrackerTask.onPostExecute: execution " + (result ? "successful" : "failed"));

            if (!result) {
                String logMessage = "Failed to load tracker data.";
                // Error loading dataset
                Log.e(LOGTAG, logMessage);
                vuforiaException = new AppException(AppException.LOADING_TRACKERS_FAILURE,
                        logMessage);
            } else {
                // Hint to the virtual machine that it would be a good time to
                // run the garbage collector:
                //
                // NOTE: This is only a hint. There is no guarantee that the
                // garbage collector will actually be run.
                System.gc();

                Vuforia.registerCallback(AppSession.this);

                started = true;
            }

            // Done loading the tracker, update application status, send the
            // exception to check errors
            controller.onInitARDone(vuforiaException);
        }
    }

    // Returns the error message for each error code
    private String getInitializationErrorString(int code) {
        if (code == Vuforia.INIT_DEVICE_NOT_SUPPORTED)
            return activity.getString(R.string.INIT_ERROR_DEVICE_NOT_SUPPORTED);
        if (code == Vuforia.INIT_NO_CAMERA_ACCESS)
            return activity.getString(R.string.INIT_ERROR_NO_CAMERA_ACCESS);
        if (code == Vuforia.INIT_LICENSE_ERROR_MISSING_KEY)
            return activity.getString(R.string.INIT_LICENSE_ERROR_MISSING_KEY);
        if (code == Vuforia.INIT_LICENSE_ERROR_INVALID_KEY)
            return activity.getString(R.string.INIT_LICENSE_ERROR_INVALID_KEY);
        if (code == Vuforia.INIT_LICENSE_ERROR_NO_NETWORK_TRANSIENT)
            return activity.getString(R.string.INIT_LICENSE_ERROR_NO_NETWORK_TRANSIENT);
        if (code == Vuforia.INIT_LICENSE_ERROR_NO_NETWORK_PERMANENT)
            return activity.getString(R.string.INIT_LICENSE_ERROR_NO_NETWORK_PERMANENT);
        if (code == Vuforia.INIT_LICENSE_ERROR_CANCELED_KEY)
            return activity.getString(R.string.INIT_LICENSE_ERROR_CANCELED_KEY);
        if (code == Vuforia.INIT_LICENSE_ERROR_PRODUCT_TYPE_MISMATCH)
            return activity.getString(R.string.INIT_LICENSE_ERROR_PRODUCT_TYPE_MISMATCH);
        else {
            return activity.getString(R.string.INIT_LICENSE_ERROR_UNKNOWN_ERROR);
        }
    }

    // Stores screen dimensions
    private void storeScreenDimensions() {
        // Query display dimensions:
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        screenWidth = metrics.widthPixels;
        screenHeight = metrics.heightPixels;
    }

    // Stores the orientation depending on the current resources configuration
    private void updateActivityOrientation() {
        Configuration config = activity.getResources().getConfiguration();

        switch (config.orientation) {
            case Configuration.ORIENTATION_PORTRAIT:
                isPortrait = true;
                break;
            case Configuration.ORIENTATION_LANDSCAPE:
                isPortrait = false;
                break;
            case Configuration.ORIENTATION_UNDEFINED:
            default:
                break;
        }

        Log.i(LOGTAG, "Activity is in " + (isPortrait ? "PORTRAIT" : "LANDSCAPE"));
    }

    // Method for setting / updating the projection matrix for AR content
    // rendering
    public void setProjectionMatrix() {
        CameraCalibration camCal = CameraDevice.getInstance().getCameraCalibration();
        projectionMatrix = Tool.getProjectionGL(camCal, 10.0f, 50000.0f);
    }


    public void stopCamera() {
        if(cameraRunning) {
            controller.doStopTrackers();
            CameraDevice.getInstance().stop();
            CameraDevice.getInstance().deinit();
            cameraRunning = false;
        }
    }


    // Applies auto focus if supported by the current device
    private boolean setFocusMode(int mode) throws AppException {
        boolean result = CameraDevice.getInstance().setFocusMode(mode);

        if (!result)
            throw new AppException(AppException.SET_FOCUS_MODE_FAILURE,
                    "Failed to set focus mode: " + mode);

        return result;
    }


    // Configures the video mode and sets offsets for the camera's image
    private void configureVideoBackground() {
        CameraDevice cameraDevice = CameraDevice.getInstance();
        VideoMode vm = cameraDevice.getVideoMode(CameraDevice.MODE.MODE_DEFAULT);

        VideoBackgroundConfig config = new VideoBackgroundConfig();
        config.setEnabled(true);
        config.setSynchronous(true);
        config.setPosition(new Vec2I(0, 0));

        int xSize = 0, ySize = 0;
        if (isPortrait) {
            xSize = (int) (vm.getHeight() * (screenHeight / (float) vm
                    .getWidth()));
            ySize = screenHeight;

            if (xSize < screenWidth) {
                xSize = screenWidth;
                ySize = (int) (screenWidth * (vm.getWidth() / (float) vm.getHeight()));
            }
        } else {
            xSize = screenWidth;
            ySize = (int) (vm.getHeight() * (screenWidth / (float) vm
                    .getWidth()));

            if (ySize < screenHeight) {
                xSize = (int) (screenHeight * (vm.getWidth() / (float) vm
                        .getHeight()));
                ySize = screenHeight;
            }
        }

        config.setSize(new Vec2I(xSize, ySize));

        Log.i(LOGTAG, "Configure Video Background : Video (" + vm.getWidth()
                + " , " + vm.getHeight() + "), Screen (" + screenWidth + " , "
                + screenHeight + "), mSize (" + xSize + " , " + ySize + ")");

        Renderer.getInstance().setVideoBackgroundConfig(config);

    }


    // Returns true if Vuforia is initialized, the trackers started and the
    // tracker data loaded
    private boolean isARRunning() {
        return started;
    }

    public void advance() {
        Log.i(LOGTAG, "Advancing");
    }
}
